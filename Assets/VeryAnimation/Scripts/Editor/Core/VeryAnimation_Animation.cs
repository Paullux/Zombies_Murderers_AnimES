﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Profiling;
using UnityEditor;
using UnityEditor.Animations;
using UnityEditorInternal;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;

namespace VeryAnimation
{
    public partial class VeryAnimation
    {
        private static readonly string[] DofIndex2String =
        {
            ".x", ".y", ".z", ".w"
        };
        public enum AnimatorIKIndex
        {
            None = -1,
            LeftHand,
            RightHand,
            LeftFoot,
            RightFoot,
            Total
        }
        public static readonly AnimatorIKIndex[] AnimatorIKMirrorIndexes =
        {
            AnimatorIKIndex.RightHand,
            AnimatorIKIndex.LeftHand,
            AnimatorIKIndex.RightFoot,
            AnimatorIKIndex.LeftFoot,
        };
        public static readonly HumanBodyBones[] AnimatorIKIndex2HumanBodyBones =
        {
            HumanBodyBones.LeftHand,
            HumanBodyBones.RightHand,
            HumanBodyBones.LeftFoot,
            HumanBodyBones.RightFoot,
        };
        public enum AnimatorTDOFIndex
        {
            None = -1,
            LeftUpperLeg,
            RightUpperLeg,
            Spine,
            Chest,
            Neck,
            LeftShoulder,
            RightShoulder,
            UpperChest,
#if UNITY_2017_3_OR_NEWER
            LeftLowerLeg,
            RightLowerLeg,
            LeftFoot,
            RightFoot,
            Head,
            LeftUpperArm,
            RightUpperArm,
            LeftLowerArm,
            RightLowerArm,
            LeftHand,
            RightHand,
            LeftToes,
            RightToes,
#endif
            Total
        }
        public static readonly AnimatorTDOFIndex[] AnimatorTDOFMirrorIndexes =
        {
            AnimatorTDOFIndex.RightUpperLeg,
            AnimatorTDOFIndex.LeftUpperLeg,
            AnimatorTDOFIndex.None,
            AnimatorTDOFIndex.None,
            AnimatorTDOFIndex.None,
            AnimatorTDOFIndex.RightShoulder,
            AnimatorTDOFIndex.LeftShoulder,
            AnimatorTDOFIndex.None,
#if UNITY_2017_3_OR_NEWER
            AnimatorTDOFIndex.RightLowerLeg,
            AnimatorTDOFIndex.LeftLowerLeg,
            AnimatorTDOFIndex.RightFoot,
            AnimatorTDOFIndex.LeftFoot,
            AnimatorTDOFIndex.None,
            AnimatorTDOFIndex.RightUpperArm,
            AnimatorTDOFIndex.LeftUpperArm,
            AnimatorTDOFIndex.RightLowerArm,
            AnimatorTDOFIndex.LeftLowerArm,
            AnimatorTDOFIndex.RightHand,
            AnimatorTDOFIndex.LeftHand,
            AnimatorTDOFIndex.RightToes,
            AnimatorTDOFIndex.LeftToes,
#endif
        };
        public static readonly HumanBodyBones[] AnimatorTDOFIndex2HumanBodyBones =
        {
            HumanBodyBones.LeftUpperLeg,
            HumanBodyBones.RightUpperLeg,
            HumanBodyBones.Spine,
            HumanBodyBones.Chest,
            HumanBodyBones.Neck,
            HumanBodyBones.LeftShoulder,
            HumanBodyBones.RightShoulder,
            HumanBodyBones.UpperChest,
#if UNITY_2017_3_OR_NEWER
            HumanBodyBones.LeftLowerLeg,
            HumanBodyBones.RightLowerLeg,
            HumanBodyBones.LeftFoot,
            HumanBodyBones.RightFoot,
            HumanBodyBones.Head,
            HumanBodyBones.LeftUpperArm,
            HumanBodyBones.RightUpperArm,
            HumanBodyBones.LeftLowerArm,
            HumanBodyBones.RightLowerArm,
            HumanBodyBones.LeftHand,
            HumanBodyBones.RightHand,
            HumanBodyBones.LeftToes,
            HumanBodyBones.RightToes,
#endif
        };

        public static readonly HumanBodyBones[] HumanBodyMirrorBones =
        {
            (HumanBodyBones)(-1),           //Hips = 0,
            HumanBodyBones.RightUpperLeg,   //LeftUpperLeg = 1,
            HumanBodyBones.LeftUpperLeg,    //RightUpperLeg = 2,
            HumanBodyBones.RightLowerLeg,   //LeftLowerLeg = 3,
            HumanBodyBones.LeftLowerLeg,    //RightLowerLeg = 4,
            HumanBodyBones.RightFoot,       //LeftFoot = 5,
            HumanBodyBones.LeftFoot,        //RightFoot = 6,
            (HumanBodyBones)(-1),           //Spine = 7,
            (HumanBodyBones)(-1),           //Chest = 8,
            (HumanBodyBones)(-1),           //Neck = 9,
            (HumanBodyBones)(-1),           //Head = 10,
            HumanBodyBones.RightShoulder,   //LeftShoulder = 11,
            HumanBodyBones.LeftShoulder,    //RightShoulder = 12,
            HumanBodyBones.RightUpperArm,   //LeftUpperArm = 13,
            HumanBodyBones.LeftUpperArm,    //RightUpperArm = 14,
            HumanBodyBones.RightLowerArm,   //LeftLowerArm = 15,
            HumanBodyBones.LeftLowerArm,    //RightLowerArm = 16,
            HumanBodyBones.RightHand,       //LeftHand = 17,
            HumanBodyBones.LeftHand,        //RightHand = 18,
            HumanBodyBones.RightToes,       //LeftToes = 19,
            HumanBodyBones.LeftToes,        //RightToes = 20,
            HumanBodyBones.RightEye,        //LeftEye = 21,
            HumanBodyBones.LeftEye,         //RightEye = 22,
            (HumanBodyBones)(-1),           //Jaw = 23,
            HumanBodyBones.RightThumbProximal,      //LeftThumbProximal = 24,
            HumanBodyBones.RightThumbIntermediate,  //LeftThumbIntermediate = 25,
            HumanBodyBones.RightThumbDistal,        //LeftThumbDistal = 26,
            HumanBodyBones.RightIndexProximal,      //LeftIndexProximal = 27,
            HumanBodyBones.RightIndexIntermediate,  //LeftIndexIntermediate = 28,
            HumanBodyBones.RightIndexDistal,        //LeftIndexDistal = 29,
            HumanBodyBones.RightMiddleProximal,     //LeftMiddleProximal = 30,
            HumanBodyBones.RightMiddleIntermediate, //LeftMiddleIntermediate = 31,
            HumanBodyBones.RightMiddleDistal,       //LeftMiddleDistal = 32,
            HumanBodyBones.RightRingProximal,       //LeftRingProximal = 33,
            HumanBodyBones.RightRingIntermediate,   //LeftRingIntermediate = 34,
            HumanBodyBones.RightRingDistal,         //LeftRingDistal = 35,
            HumanBodyBones.RightLittleProximal,     //LeftLittleProximal = 36,
            HumanBodyBones.RightLittleIntermediate, //LeftLittleIntermediate = 37,
            HumanBodyBones.RightLittleDistal,       //LeftLittleDistal = 38,
            HumanBodyBones.LeftThumbProximal,       //RightThumbProximal = 39,
            HumanBodyBones.LeftThumbIntermediate,   //RightThumbIntermediate = 40,
            HumanBodyBones.LeftThumbDistal,         //RightThumbDistal = 41,
            HumanBodyBones.LeftIndexProximal,       //RightIndexProximal = 42,
            HumanBodyBones.LeftIndexIntermediate,   //RightIndexIntermediate = 43,
            HumanBodyBones.LeftIndexDistal,         //RightIndexDistal = 44,
            HumanBodyBones.LeftMiddleProximal,      //RightMiddleProximal = 45,
            HumanBodyBones.LeftMiddleIntermediate,  //RightMiddleIntermediate = 46,
            HumanBodyBones.LeftMiddleDistal,        //RightMiddleDistal = 47,
            HumanBodyBones.LeftRingProximal,        //RightRingProximal = 48,
            HumanBodyBones.LeftRingIntermediate,    //RightRingIntermediate = 49,
            HumanBodyBones.LeftRingDistal,          //RightRingDistal = 50,
            HumanBodyBones.LeftLittleProximal,      //RightLittleProximal = 51,
            HumanBodyBones.LeftLittleIntermediate,  //RightLittleIntermediate = 52,
            HumanBodyBones.LeftLittleDistal,        //RightLittleDistal = 53,
            (HumanBodyBones)(-1),                   //UpperChest = 54,
        };

        public class HumanVirtualBone
        {
            public HumanBodyBones boneA;
            public HumanBodyBones boneB;
            public float leap;
            public Quaternion addRotation = Quaternion.identity;
            public Vector3 limitSign = Vector3.one;
        }
        public static readonly HumanVirtualBone[][] HumanVirtualBones =
        {
            null, //Hips = 0,
            null, //LeftUpperLeg = 1,
            null, //RightUpperLeg = 2,
            null, //LeftLowerLeg = 3,
            null, //RightLowerLeg = 4,
            null, //LeftFoot = 5,
            null, //RightFoot = 6,
            null, //Spine = 7,
            new HumanVirtualBone[] { new HumanVirtualBone() { boneA = HumanBodyBones.Spine, boneB = HumanBodyBones.Head, leap = 0.15f } }, //Chest = 8,
            new HumanVirtualBone[] { new HumanVirtualBone() { boneA = HumanBodyBones.UpperChest, boneB = HumanBodyBones.Head, leap = 0.8f },
                                        new HumanVirtualBone() { boneA = HumanBodyBones.Chest, boneB = HumanBodyBones.Head, leap = 0.8f },
                                        new HumanVirtualBone() { boneA = HumanBodyBones.Spine, boneB = HumanBodyBones.Head, leap = 0.85f } }, //Neck = 9,
            null, //Head = 10,
            new HumanVirtualBone[] { new HumanVirtualBone() { boneA = HumanBodyBones.LeftUpperArm, boneB = HumanBodyBones.RightUpperArm, leap = 0.2f, limitSign = new Vector3(1f, 1f, -1f) } }, //LeftShoulder = 11,
            new HumanVirtualBone[] { new HumanVirtualBone() { boneA = HumanBodyBones.RightUpperArm, boneB = HumanBodyBones.LeftUpperArm, leap = 0.2f } }, //RightShoulder = 12,
            null, //LeftUpperArm = 13,
            null, //RightUpperArm = 14,
            null, //LeftLowerArm = 15,
            null, //RightLowerArm = 16,
            null, //LeftHand = 17,
            null, //RightHand = 18,
            null, //LeftToes = 19,
            null, //RightToes = 20,
            null, //LeftEye = 21,
            null, //RightEye = 22,
            null, //Jaw = 23,
            null, //LeftThumbProximal = 24,
            null, //LeftThumbIntermediate = 25,
            null, //LeftThumbDistal = 26,
            null, //LeftIndexProximal = 27,
            null, //LeftIndexIntermediate = 28,
            null, //LeftIndexDistal = 29,
            null, //LeftMiddleProximal = 30,
            null, //LeftMiddleIntermediate = 31,
            null, //LeftMiddleDistal = 32,
            null, //LeftRingProximal = 33,
            null, //LeftRingIntermediate = 34,
            null, //LeftRingDistal = 35,
            null, //LeftLittleProximal = 36,
            null, //LeftLittleIntermediate = 37,
            null, //LeftLittleDistal = 38,
            null, //RightThumbProximal = 39,
            null, //RightThumbIntermediate = 40,
            null, //RightThumbDistal = 41,
            null, //RightIndexProximal = 42,
            null, //RightIndexIntermediate = 43,
            null, //RightIndexDistal = 44,
            null, //RightMiddleProximal = 45,
            null, //RightMiddleIntermediate = 46,
            null, //RightMiddleDistal = 47,
            null, //RightRingProximal = 48,
            null, //RightRingIntermediate = 49,
            null, //RightRingDistal = 50,
            null, //RightLittleProximal = 51,
            null, //RightLittleIntermediate = 52,
            null, //RightLittleDistal = 53,
            new HumanVirtualBone[] { new HumanVirtualBone() { boneA = HumanBodyBones.Chest, boneB = HumanBodyBones.Head, leap = 0.2f },
                                        new HumanVirtualBone() { boneA = HumanBodyBones.Spine, boneB = HumanBodyBones.Head, leap = 0.3f } }, //UpperChest = 54,
        };

        public class AnimatorTDOF
        {
            public AnimatorTDOFIndex index;
            public HumanBodyBones parent;
            public Vector3 mirror = new Vector3(1f, 1f, -1f);
        }
#if UNITY_2017_3_OR_NEWER
        public static readonly AnimatorTDOF[] HumanBonesAnimatorTDOFIndex =
        {
            null, //Hips = 0,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftUpperLeg, parent = HumanBodyBones.Hips }, //LeftUpperLeg = 1,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightUpperLeg, parent = HumanBodyBones.Hips }, //RightUpperLeg = 2,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftLowerLeg, parent = HumanBodyBones.LeftUpperLeg }, //LeftLowerLeg = 3,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightLowerLeg, parent = HumanBodyBones.RightUpperLeg }, //RightLowerLeg = 4,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftFoot, parent = HumanBodyBones.LeftLowerLeg }, //LeftFoot = 5,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightFoot, parent = HumanBodyBones.RightLowerLeg }, //RightFoot = 6,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Spine, parent = HumanBodyBones.Hips }, //Spine = 7,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Chest, parent = HumanBodyBones.Spine }, //Chest = 8,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Neck, parent = HumanBodyBones.UpperChest }, //Neck = 9,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Head, parent = HumanBodyBones.Neck }, //Head = 10,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftShoulder, parent = HumanBodyBones.Chest }, //LeftShoulder = 11,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightShoulder, parent = HumanBodyBones.Chest }, //RightShoulder = 12,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftUpperArm, parent = HumanBodyBones.LeftShoulder, mirror = new Vector3(1f, -1f, 1f) }, //LeftUpperArm = 13,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightUpperArm, parent = HumanBodyBones.RightShoulder, mirror = new Vector3(1f, -1f, 1f) }, //RightUpperArm = 14,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftLowerArm, parent = HumanBodyBones.LeftUpperArm, mirror = new Vector3(1f, -1f, 1f)  }, //LeftLowerArm = 15,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightLowerArm, parent = HumanBodyBones.RightUpperArm, mirror = new Vector3(1f, -1f, 1f)  }, //RightLowerArm = 16,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftHand, parent = HumanBodyBones.LeftLowerArm, mirror = new Vector3(1f, -1f, 1f)  }, //LeftHand = 17,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightHand, parent = HumanBodyBones.RightLowerArm, mirror = new Vector3(1f, -1f, 1f)  }, //RightHand = 18,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftToes, parent = HumanBodyBones.LeftFoot }, //LeftToes = 19,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightToes, parent = HumanBodyBones.RightFoot }, //RightToes = 20,
            null, //LeftEye = 21,
            null, //RightEye = 22,
            null, //Jaw = 23,
            null, //LeftThumbProximal = 24,
            null, //LeftThumbIntermediate = 25,
            null, //LeftThumbDistal = 26,
            null, //LeftIndexProximal = 27,
            null, //LeftIndexIntermediate = 28,
            null, //LeftIndexDistal = 29,
            null, //LeftMiddleProximal = 30,
            null, //LeftMiddleIntermediate = 31,
            null, //LeftMiddleDistal = 32,
            null, //LeftRingProximal = 33,
            null, //LeftRingIntermediate = 34,
            null, //LeftRingDistal = 35,
            null, //LeftLittleProximal = 36,
            null, //LeftLittleIntermediate = 37,
            null, //LeftLittleDistal = 38,
            null, //RightThumbProximal = 39,
            null, //RightThumbIntermediate = 40,
            null, //RightThumbDistal = 41,
            null, //RightIndexProximal = 42,
            null, //RightIndexIntermediate = 43,
            null, //RightIndexDistal = 44,
            null, //RightMiddleProximal = 45,
            null, //RightMiddleIntermediate = 46,
            null, //RightMiddleDistal = 47,
            null, //RightRingProximal = 48,
            null, //RightRingIntermediate = 49,
            null, //RightRingDistal = 50,
            null, //RightLittleProximal = 51,
            null, //RightLittleIntermediate = 52,
            null, //RightLittleDistal = 53,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.UpperChest, parent = HumanBodyBones.Chest }, //UpperChest = 54,
        };
#else
        public static readonly AnimatorTDOF[] HumanBonesAnimatorTDOFIndex =
        {
            null, //Hips = 0,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftUpperLeg, parent = HumanBodyBones.Hips }, //LeftUpperLeg = 1,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightUpperLeg, parent = HumanBodyBones.Hips }, //RightUpperLeg = 2,
            null, //LeftLowerLeg = 3,
            null, //RightLowerLeg = 4,
            null, //LeftFoot = 5,
            null, //RightFoot = 6,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Spine, parent = HumanBodyBones.Hips }, //Spine = 7,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Chest, parent = HumanBodyBones.Spine }, //Chest = 8,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.Neck, parent = HumanBodyBones.UpperChest }, //Neck = 9,
            null, //Head = 10,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.LeftShoulder, parent = HumanBodyBones.Chest }, //LeftShoulder = 11,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.RightShoulder, parent = HumanBodyBones.Chest }, //RightShoulder = 12,
            null, //LeftUpperArm = 13,
            null, //RightUpperArm = 14,
            null, //LeftLowerArm = 15,
            null, //RightLowerArm = 16,
            null, //LeftHand = 17,
            null, //RightHand = 18,
            null, //LeftToes = 19,
            null, //RightToes = 20,
            null, //LeftEye = 21,
            null, //RightEye = 22,
            null, //Jaw = 23,
            null, //LeftThumbProximal = 24,
            null, //LeftThumbIntermediate = 25,
            null, //LeftThumbDistal = 26,
            null, //LeftIndexProximal = 27,
            null, //LeftIndexIntermediate = 28,
            null, //LeftIndexDistal = 29,
            null, //LeftMiddleProximal = 30,
            null, //LeftMiddleIntermediate = 31,
            null, //LeftMiddleDistal = 32,
            null, //LeftRingProximal = 33,
            null, //LeftRingIntermediate = 34,
            null, //LeftRingDistal = 35,
            null, //LeftLittleProximal = 36,
            null, //LeftLittleIntermediate = 37,
            null, //LeftLittleDistal = 38,
            null, //RightThumbProximal = 39,
            null, //RightThumbIntermediate = 40,
            null, //RightThumbDistal = 41,
            null, //RightIndexProximal = 42,
            null, //RightIndexIntermediate = 43,
            null, //RightIndexDistal = 44,
            null, //RightMiddleProximal = 45,
            null, //RightMiddleIntermediate = 46,
            null, //RightMiddleDistal = 47,
            null, //RightRingProximal = 48,
            null, //RightRingIntermediate = 49,
            null, //RightRingDistal = 50,
            null, //RightLittleProximal = 51,
            null, //RightLittleIntermediate = 52,
            null, //RightLittleDistal = 53,
            new AnimatorTDOF() { index = AnimatorTDOFIndex.UpperChest, parent = HumanBodyBones.Chest }, //UpperChest = 54,
        };
#endif

        #region AnimatorRootCorrection
        private struct AnimatorRootCorrection
        {
            public bool save;
            public bool update;
            public Vector3 hipBeforePos;
            public Quaternion hipBeforeRot;
        }
        private AnimatorRootCorrection updateAnimatorRootCorrection;
        public void SaveAnimatorRootCorrection()
        {
            if (!isHuman) return;
            var tGO = editGameObject.transform;
            var tHip = editHumanoidBones[(int)HumanBodyBones.Hips].transform;
            var rotGOInv = Quaternion.Inverse(tGO.rotation);
            var preHipInverse = Quaternion.Inverse(uAvatar.GetPreRotation(editAnimator.avatar, (int)HumanBodyBones.Hips));
            var postHip = uAvatar.GetPostRotation(editAnimator.avatar, (int)HumanBodyBones.Hips);
            updateAnimatorRootCorrection.hipBeforePos = tHip.position;
            updateAnimatorRootCorrection.hipBeforeRot = (rotGOInv * tHip.rotation * postHip) * preHipInverse;
            updateAnimatorRootCorrection.save = true;
        }
        public void EnableAnimatorRootCorrection()
        {
            updateAnimatorRootCorrection.update = true;
        }
        public void DisableAnimatorRootCorrection()
        {
            updateAnimatorRootCorrection.update = false;
            updateAnimatorRootCorrection.save = false;
        }
        private void UpdateAnimatorRootCorrection(AnimationClip clip, float time)
        {
            if (updateAnimatorRootCorrection.save && updateAnimatorRootCorrection.update)
            {
                ResampleAnimation(clip, time);
                var tGO = editGameObject.transform;
                var tHip = editHumanoidBones[(int)HumanBodyBones.Hips].transform;
                var rotGOInv = Quaternion.Inverse(tGO.rotation);
                var preHipInverse = Quaternion.Inverse(uAvatar.GetPreRotation(editAnimator.avatar, (int)HumanBodyBones.Hips));
                var postHip = uAvatar.GetPostRotation(editAnimator.avatar, (int)HumanBodyBones.Hips);
                {
                    var hipAfterRot = (rotGOInv * tHip.rotation * postHip) * preHipInverse;
                    {
                        var offset = Quaternion.Inverse(hipAfterRot * Quaternion.Inverse(updateAnimatorRootCorrection.hipBeforeRot));
                        if (!Mathf.Approximately(offset.x, Quaternion.identity.x) ||
                            !Mathf.Approximately(offset.y, Quaternion.identity.y) ||
                            !Mathf.Approximately(offset.z, Quaternion.identity.z) ||
                            !Mathf.Approximately(offset.w, Quaternion.identity.w))
                        {
                            SetAnimationCurveAnimatorRootQ(offset * GetAnimationCurveAnimatorRootQ(), time);
                            ResampleAnimation(clip, time);
                        }
                    }
                }
                var hipAfterPos = tHip.position;
                {
                    var offset = (Quaternion.Inverse(tGO.rotation) * (hipAfterPos - updateAnimatorRootCorrection.hipBeforePos)) * (1f / vaw.animator.humanScale);
                    for (int i = 0; i < 3; i++)
                    {
                        if (tGO.lossyScale[i] != 0f)
                            offset[i] *= 1f / tGO.lossyScale[i];
                    }
                    if (offset.sqrMagnitude > 0f)
                    {
                        SetAnimationCurveAnimatorRootT(GetAnimationCurveAnimatorRootT() - offset, time);
                    }
                }
            }
            DisableAnimatorRootCorrection();
        }
        #endregion

        #region AnimationTool
        public HumanBodyBones GetHumanVirtualBoneParentBone(HumanBodyBones bone)
        {
            if (!isHuman) return (HumanBodyBones)(-1);
            var vbs = HumanVirtualBones[(int)bone];
            if (vbs != null)
            {
                foreach (var vb in vbs)
                {
                    if (humanoidBones[(int)vb.boneA] == null) continue;
                    return vb.boneA;
                }
            }
            return (HumanBodyBones)(-1);
        }
        public Vector3 GetHumanVirtualBoneLimitSign(HumanBodyBones bone)
        {
            if (!isHuman) return Vector3.one;
            var vbs = HumanVirtualBones[(int)bone];
            if (vbs != null)
            {
                foreach (var vb in vbs)
                {
                    if (humanoidBones[(int)vb.boneA] == null) continue;
                    return vb.limitSign;
                }
            }
            return Vector3.one;
        }

        public Vector3 GetHumanVirtualBonePosition(HumanBodyBones bone)
        {
            if (!isHuman) return Vector3.zero;
            var vbs = HumanVirtualBones[(int)bone];
            if (vbs != null)
            {
                foreach (var vb in vbs)
                {
                    if (editHumanoidBones[(int)vb.boneA] == null || editHumanoidBones[(int)vb.boneB] == null) continue;
                    var posA = editHumanoidBones[(int)vb.boneA].transform.position;
                    var posB = editHumanoidBones[(int)vb.boneB].transform.position;
                    return Vector3.Lerp(posA, posB, vb.leap);
                }
            }
            return Vector3.zero;
        }
        public Quaternion GetHumanVirtualBoneRotation(HumanBodyBones bone)
        {
            if (!isHuman) return Quaternion.identity;
            var vbs = HumanVirtualBones[(int)bone];
            if (vbs != null)
            {
                foreach (var vb in vbs)
                {
                    if (editHumanoidBones[(int)vb.boneA] == null) continue;
                    var vRotation = Vector3.zero;
                    for (int i = 0; i < 3; i++)
                    {
                        var mi = HumanTrait.MuscleFromBone((int)bone, i);
                        if (i >= 0)
                        {
                            var muscle = GetAnimationCurveAnimatorMuscle(mi);
                            vRotation[i] = Mathf.Lerp(humanoidMuscleLimit[(int)bone].min[i], humanoidMuscleLimit[(int)bone].max[i], (muscle + 1f) / 2f);
                        }
                    }
                    var qRotation = Quaternion.Euler(vRotation);
                    var parentRotation = editHumanoidBones[(int)vb.boneA].transform.rotation * uAvatar.GetPostRotation(editAnimator.avatar, (int)vb.boneA);
                    return parentRotation * qRotation;
                }
            }
            return Quaternion.identity;
        }
        public Quaternion GetHumanVirtualBoneParentRotation(HumanBodyBones bone)
        {
            if (!isHuman) return Quaternion.identity;
            var vbs = HumanVirtualBones[(int)bone];
            if (vbs != null)
            {
                foreach (var vb in vbs)
                {
                    if (editHumanoidBones[(int)vb.boneA] == null) continue;
                    return editHumanoidBones[(int)vb.boneA].transform.rotation * uAvatar.GetPostRotation(editAnimator.avatar, (int)vb.boneA) * vb.addRotation;
                }
            }
            return Quaternion.identity;
        }

        public Vector3 GetHumanWorldRootPosition()
        {
            if (!isHuman) return Vector3.zero;
            var bodyPosition = GetAnimationCurveAnimatorRootT() * editAnimator.humanScale;
            return editGameObject.transform.localToWorldMatrix.MultiplyPoint3x4(bodyPosition);
        }
        public Vector3 GetHumanLocalRootPosition(Vector3 pos)
        {
            if (!isHuman) return Vector3.zero;
            var bodyPosition = editGameObject.transform.worldToLocalMatrix.MultiplyPoint3x4(pos);
            return bodyPosition / editAnimator.humanScale;
        }
        public Quaternion GetHumanWorldRootRotation()
        {
            if (!isHuman) return Quaternion.identity;
            return editGameObject.transform.rotation * GetAnimationCurveAnimatorRootQ();
        }
        public Quaternion GetHumanLocalRootRotation(Quaternion rot)
        {
            if (!isHuman) return Quaternion.identity;
            return Quaternion.Inverse(editGameObject.transform.rotation) * rot;
        }

        public bool GetPlayingAnimationInfo(out AnimationClip dstClip, out float dstTime)
        {
            dstClip = null;
            dstTime = 0f;
            if (!EditorApplication.isPlaying) return false;
            if (vaw.animator != null && vaw.animator.runtimeAnimatorController != null && vaw.animator.isInitialized)
            {
                #region animator
                UnityEditor.Animations.AnimatorController ac = null;
                AnimatorOverrideController owc = null;
                if (vaw.animator.runtimeAnimatorController is AnimatorOverrideController)
                {
                    owc = vaw.animator.runtimeAnimatorController as AnimatorOverrideController;
                    ac = owc.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
                }
                else
                {
                    ac = vaw.animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
                }
                if (vaw.animator.layerCount > 0)
                {
                    var layerIndex = 0;
                    var currentAnimatorStateInfo = vaw.animator.GetCurrentAnimatorStateInfo(layerIndex);
                    AnimationClip resultClip = null;
                    float resultTime = 0f;
                    Func<AnimatorStateMachine, bool> FindStateMachine = null;
                    FindStateMachine = (stateMachine) =>
                    {
                        foreach (var state in stateMachine.states)
                        {
                            if (state.state.nameHash != currentAnimatorStateInfo.shortNameHash ||
                                currentAnimatorStateInfo.length <= 0f)
                                continue;

                            Func<Motion, AnimationClip> FindMotion = null;
                            FindMotion = (motion) =>
                            {
                                if (motion != null)
                                {
                                    if (motion is UnityEditor.Animations.BlendTree)
                                    {
                                        #region BlendTree
                                        var blendTree = motion as UnityEditor.Animations.BlendTree;
                                        switch (blendTree.blendType)
                                        {
                                        case BlendTreeType.Simple1D:
                                            #region 1D
                                            {
                                                var param = vaw.animator.GetFloat(blendTree.blendParameter);
                                                float near = float.MaxValue;
                                                int index = -1;
                                                for (int i = 0; i < blendTree.children.Length; i++)
                                                {
                                                    var offset = Mathf.Abs(blendTree.children[i].threshold - param);
                                                    if (offset < near)
                                                    {
                                                        index = i;
                                                        near = offset;
                                                    }
                                                }
                                                if (index >= 0)
                                                {
                                                    return FindMotion(blendTree.children[index].motion);
                                                }
                                            }
                                            #endregion
                                            break;
                                        case BlendTreeType.SimpleDirectional2D:
                                        case BlendTreeType.FreeformDirectional2D:
                                        case BlendTreeType.FreeformCartesian2D:
                                            #region 2D
                                            {
                                                var paramX = vaw.animator.GetFloat(blendTree.blendParameter);
                                                var paramY = vaw.animator.GetFloat(blendTree.blendParameterY);
                                                float near = float.MaxValue;
                                                int index = -1;
                                                for (int i = 0; i < blendTree.children.Length; i++)
                                                {
                                                    var offsetX = Mathf.Abs(blendTree.children[i].position.x - paramX);
                                                    var offsetY = Mathf.Abs(blendTree.children[i].position.y - paramY);
                                                    if (offsetX + offsetY < near)
                                                    {
                                                        index = i;
                                                        near = offsetX + offsetY;
                                                    }
                                                }
                                                if (index >= 0)
                                                {
                                                    return FindMotion(blendTree.children[index].motion);
                                                }
                                            }
                                            #endregion
                                            break;
                                        case BlendTreeType.Direct:
                                            #region Direct
                                            {
                                                float max = float.MinValue;
                                                int index = -1;
                                                for (int i = 0; i < blendTree.children.Length; i++)
                                                {
                                                    var param = vaw.animator.GetFloat(blendTree.children[i].directBlendParameter);
                                                    if (param >= max)
                                                    {
                                                        index = i;
                                                        max = param;
                                                    }
                                                }
                                                if (index >= 0)
                                                {
                                                    return FindMotion(blendTree.children[index].motion);
                                                }
                                            }
                                            #endregion
                                            break;
                                        default:
                                            Assert.IsTrue(false, "not support type");
                                            break;
                                        }
                                        #endregion
                                    }
                                    else if (motion is AnimationClip)
                                    {
                                        return motion as AnimationClip;
                                    }
                                }
                                return null;
                            };
                            var clip = FindMotion(state.state.motion);
                            if (clip == null)
                                continue;
                            if (owc != null)
                                clip = owc[clip];
                            resultClip = clip;
                            var time = currentAnimatorStateInfo.length * currentAnimatorStateInfo.normalizedTime;
                            resultTime = time;
                            if (resultClip.isLooping)
                            {
                                var loop = Mathf.FloorToInt(time / currentAnimatorStateInfo.length);
                                resultTime -= loop * currentAnimatorStateInfo.length;
                            }
                            else if (resultTime > currentAnimatorStateInfo.length)
                            {
                                resultTime = currentAnimatorStateInfo.length;
                            }
                            return true;
                        }
                        foreach (var cstateMachine in stateMachine.stateMachines)
                        {
                            if (FindStateMachine(cstateMachine.stateMachine))
                                return true;
                        }
                        return false;
                    };
                    if (FindStateMachine(ac.layers[layerIndex].stateMachine))
                    {
                        dstClip = resultClip;
                        dstTime = resultTime;
                        return true;
                    }
                }
                #endregion
            }
            else if (vaw.animation != null)
            {
                #region animation
                foreach (AnimationState state in vaw.animation)
                {
                    if (!state.enabled || state.length <= 0f) continue;
                    dstClip = state.clip;
                    var time = state.time;
                    dstTime = time;
                    switch (state.wrapMode)
                    {
                    case WrapMode.Loop:
                        {
                            var loop = Mathf.FloorToInt(time / state.length);
                            dstTime -= loop * state.length;
                        }
                        break;
                    case WrapMode.PingPong:
                        {
                            var loop = Mathf.FloorToInt(time / state.length);
                            dstTime -= loop * state.length;
                            if (loop % 2 != 0)
                                dstTime = state.length - dstTime;
                        }
                        break;
                    default:
                        dstTime = Mathf.Min(dstTime, state.length);
                        break;
                    }
                    return true;
                }
                #endregion
            }
            return false;
        }

        public enum HumanoidBoneAttribute
        {
            Center,
            Left,
            Right,
        }
        public HumanoidBoneAttribute GetHumanoidBoneAttribute(HumanBodyBones bone)
        {
            var text = bone.ToString();
            if (text.StartsWith("Left")) return HumanoidBoneAttribute.Left;
            else if (text.StartsWith("Right")) return HumanoidBoneAttribute.Right;
            else return HumanoidBoneAttribute.Center;
        }

        public int GetMirrorMuscleIndex(int muscleIndex)
        {
            if (muscleIndex < 0) return -1;
            var humanIndex = HumanTrait.BoneFromMuscle(muscleIndex);
            if (humanIndex < 0) return -1;
            if (HumanBodyMirrorBones[humanIndex] < 0) return -1;
            for (int i = 0; i < 3; i++)
            {
                if (muscleIndex == HumanTrait.MuscleFromBone(humanIndex, i))
                    return HumanTrait.MuscleFromBone((int)HumanBodyMirrorBones[humanIndex], i);
            }
            return -1;
        }
        private Vector3 GetMirrorBoneLocalPosition(int boneIndex, Vector3 localPosition)
        {
            var rootInv = Quaternion.Inverse(boneSaveTransforms[0].rotation);
            var local = localPosition - boneSaveTransforms[boneIndex].localPosition;
            var parentRot = (rootInv * boneSaveTransforms[boneIndex].rotation) * Quaternion.Inverse(boneSaveTransforms[boneIndex].localRotation);
            var world = parentRot * local;
            world.x = -world.x;
            if (mirrorBoneIndexes[boneIndex] >= 0)
            {
                var mparentRot = (rootInv * boneSaveTransforms[mirrorBoneIndexes[boneIndex]].rotation) * Quaternion.Inverse(boneSaveTransforms[mirrorBoneIndexes[boneIndex]].localRotation);
                var mlocal = Quaternion.Inverse(mparentRot) * world;
                return boneSaveTransforms[mirrorBoneIndexes[boneIndex]].localPosition + mlocal;
            }
            else
            {
                var mlocal = Quaternion.Inverse(parentRot) * world;
                return boneSaveTransforms[boneIndex].localPosition + mlocal;
            }
        }
        private Quaternion GetMirrorBoneLocalRotation(int boneIndex, Quaternion localRotation)
        {
            var rootInv = Quaternion.Inverse(boneSaveTransforms[0].rotation);
            var parentRot = (rootInv * boneSaveTransforms[boneIndex].rotation) * Quaternion.Inverse(boneSaveTransforms[boneIndex].localRotation);
            var wrot = parentRot * localRotation;
            if (mirrorBoneIndexes[boneIndex] >= 0)
            {
                var rootRot = rootInv * boneSaveTransforms[mirrorBoneRootIndexes[boneIndex]].rotation;
                wrot *= Quaternion.Inverse(Quaternion.Inverse(rootRot) * (rootInv * boneSaveTransforms[boneIndex].rotation));
                {
                    wrot *= Quaternion.Inverse(rootRot);
                    wrot = new Quaternion(wrot.x, -wrot.y, -wrot.z, wrot.w);
                    wrot *= rootRot;
                }
                wrot *= Quaternion.Inverse(rootRot) * (rootInv * boneSaveTransforms[mirrorBoneIndexes[boneIndex]].rotation);
                var mparentRot = (rootInv * boneSaveTransforms[mirrorBoneIndexes[boneIndex]].rotation) * Quaternion.Inverse(boneSaveTransforms[mirrorBoneIndexes[boneIndex]].localRotation);
                return Quaternion.Inverse(mparentRot) * wrot;
            }
            else
            {
                var rootRot = rootInv * boneSaveTransforms[boneIndex].rotation;
                wrot *= Quaternion.Inverse(rootRot);
                wrot = new Quaternion(wrot.x, -wrot.y, -wrot.z, wrot.w);
                wrot *= rootRot;
                return Quaternion.Inverse(parentRot) * wrot;
            }
        }
        private Vector3 GetMirrorBoneLocalScale(int boneIndex, Vector3 localRotation)
        {
            return localRotation;
        }

        private int FindKeyframeAtTime(AnimationCurve curve, float time)
        {
            if (curve.length > 0)
            {
                int begin = 0, end = curve.length - 1;
                while (end - begin > 1)
                {
                    var index = begin + Mathf.FloorToInt((end - begin) / 2f);
                    if (time < curve[index].time)
                    {
                        if (end == index) break;
                        end = index;
                    }
                    else
                    {
                        if (begin == index) break;
                        begin = index;
                    }
                }
                if (Mathf.Abs(curve[begin].time - time) < 0.0001f)
                    return begin;
                if (Mathf.Abs(curve[end].time - time) < 0.0001f)
                    return end;
            }
            return -1;
        }
        private int FindKeyframeAtTime(List<ObjectReferenceKeyframe> keys, float time)
        {
            if (keys.Count > 0)
            {
                int begin = 0, end = keys.Count - 1;
                while (end - begin > 1)
                {
                    var index = begin + Mathf.FloorToInt((end - begin) / 2f);
                    if (time < keys[index].time)
                    {
                        if (end == index) break;
                        end = index;
                    }
                    else
                    {
                        if (begin == index) break;
                        begin = index;
                    }
                }
                if (Mathf.Abs(keys[begin].time - time) < 0.0001f)
                    return begin;
                if (Mathf.Abs(keys[end].time - time) < 0.0001f)
                    return end;
            }
            return -1;
        }
        private int FindBeforeNearKeyframeAtTime(AnimationCurve curve, float time)
        {
            if (curve.length == 0) return -1;
            if (curve[curve.length - 1].time < time)
                return curve.length - 1;
            for (int i = 1; i < curve.length; i++)
            {
                if (curve[i].time >= time)
                    return i - 1;
            }
            return 0;
        }
        private int FindBeforeNearKeyframeAtTime(ObjectReferenceKeyframe[] keys, float time)
        {
            if (keys.Length == 0) return -1;
            if (keys[keys.Length - 1].time < time)
                return keys.Length - 1;
            for (int i = 1; i < keys.Length; i++)
            {
                if (keys[i].time >= time)
                    return i - 1;
            }
            return 0;
        }
        private int AddKeyframe(AnimationCurve curve, float time, float value)
        {
            var keyIndex = curve.AddKey(new Keyframe(time, value));
            if (keyIndex < 0) return -1;
            uCurveUtility.SetKeyModeFromContext(curve, keyIndex);
            uAnimationUtility.UpdateTangentsFromModeSurrounding(curve, keyIndex);
            return keyIndex;
        }
        private int AddKeyframe(AnimationCurve curve, Keyframe keyframe)
        {
            var keyIndex = curve.AddKey(keyframe);
            if (keyIndex < 0) return -1;
            uCurveUtility.SetKeyModeFromContext(curve, keyIndex);
            uAnimationUtility.UpdateTangentsFromModeSurrounding(curve, keyIndex);
            return keyIndex;
        }
        private void SetKeyframeLinear(AnimationCurve curve, int keyIndex)
        {
            if (keyIndex < 0 || keyIndex >= curve.length) return;
            AnimationUtility.SetKeyRightTangentMode(curve, keyIndex, AnimationUtility.TangentMode.Linear);
            AnimationUtility.SetKeyLeftTangentMode(curve, keyIndex, AnimationUtility.TangentMode.Linear);
            AnimationUtility.SetKeyBroken(curve, keyIndex, false);
        }
        private bool FixReverseRotationEuler(AnimationCurve curve, int keyIndex)
        {
            if (keyIndex < 1 || keyIndex >= curve.length)
                return false;
            var keyframe = curve[keyIndex];
            while (Mathf.Abs(keyframe.value - curve[keyIndex - 1].value) > 180f)
            {
                if (keyframe.value < curve[keyIndex - 1].value)
                    keyframe.value += 360f;
                else
                    keyframe.value -= 360f;
            }
            curve.MoveKey(keyIndex, keyframe);
            return true;
        }

        private UnityEditor.Animations.AnimatorController GetAnimatorController()
        {
            UnityEditor.Animations.AnimatorController ac = null;
            if (vaw.animator != null)
            {
                if (vaw.animator.runtimeAnimatorController is AnimatorOverrideController)
                {
                    var owc = vaw.animator.runtimeAnimatorController as AnimatorOverrideController;
                    ac = owc.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
                }
                else
                {
                    ac = vaw.animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
                }
            }
            return ac;
        }
        private void ActionAllAnimatorState(AnimationClip clip, Action<UnityEditor.Animations.AnimatorState> action)
        {
            var ac = GetAnimatorController();
            if (ac == null) return;

            foreach (UnityEditor.Animations.AnimatorControllerLayer layer in ac.layers)
            {
                Action<AnimatorStateMachine> ActionStateMachine = null;
                ActionStateMachine = (stateMachine) =>
                {
                    foreach (var state in stateMachine.states)
                    {
                        if (state.state.motion is UnityEditor.Animations.BlendTree)
                        {
                            Action<UnityEditor.Animations.BlendTree> ActionBlendTree = null;
                            ActionBlendTree = (blendTree) =>
                            {
                                if (blendTree.children == null) return;
                                var children = blendTree.children;
                                for (int j = 0; j < children.Length; j++)
                                {
                                    if (children[j].motion is UnityEditor.Animations.BlendTree)
                                    {
                                        ActionBlendTree(children[j].motion as UnityEditor.Animations.BlendTree);
                                    }
                                    else
                                    {
                                        if (children[j].motion == clip)
                                        {
                                            action(state.state);
                                        }
                                    }
                                }
                            };
                            ActionBlendTree(state.state.motion as UnityEditor.Animations.BlendTree);
                        }
                        else
                        {
                            if (state.state.motion == clip)
                            {
                                action(state.state);
                            }
                        }
                    }
                    foreach (var childStateMachine in stateMachine.stateMachines)
                    {
                        ActionStateMachine(childStateMachine.stateMachine);
                    }
                };
                ActionStateMachine(layer.stateMachine);
            }
        }

        private Transform GetTransformFromPath(string path)
        {
            var root = editGameObject.transform;
            if (!string.IsNullOrEmpty(path))
            {
                var splits = path.Split('/');
                for (int i = 0; i < splits.Length; i++)
                {
                    bool contains = false;
                    for (int j = 0; j < root.childCount; j++)
                    {
                        if (root.GetChild(j).name == splits[i])
                        {
                            root = root.GetChild(j);
                            contains = true;
                            break;
                        }
                    }
                    if (!contains) return null;
                }
            }
            return root;
        }
        #endregion

        private bool OnCurveWasModifiedStop = false;
        private Dictionary<EditorCurveBinding, AnimationUtility.CurveModifiedType> curvesWasModified = new Dictionary<EditorCurveBinding, AnimationUtility.CurveModifiedType>();
        private void OnCurveWasModified(AnimationClip clip, EditorCurveBinding binding, AnimationUtility.CurveModifiedType deleted)
        {
            if (isEditError || OnCurveWasModifiedStop) return;

            #region RemoveCurveCache
            {
                if (deleted == AnimationUtility.CurveModifiedType.CurveModified ||
                    deleted == AnimationUtility.CurveModifiedType.CurveDeleted)
                {
                    if (clip != editorCurveCacheClip)
                    {
                        ClearEditorCurveCache();
                    }
                    else if (editorCurveCacheDic != null)
                    {
                        if (editorCurveCacheDic.ContainsKey(GetEditorCurveBindingHashCode(binding)))
                        {
                            RemoveEditorCurveCache(clip, binding);
                        }
                        if (binding.type == typeof(Transform))
                        {
                            if (IsTransformPositionCurveBinding(binding))
                            {
                                for (int dof = 0; dof < EditorCurveBindingTransformPositionPropertyNames.Length; dof++)
                                {
                                    var tmpBinding = binding;
                                    tmpBinding.propertyName = EditorCurveBindingTransformPositionPropertyNames[dof];
                                    RemoveEditorCurveCache(clip, tmpBinding);
                                }
                            }
                            else if (IsTransformRotationCurveBinding(binding))
                            {
                                for (int mode = 0; mode < (int)URotationCurveInterpolation.Mode.Undefined; mode++)
                                {
                                    for (int dof = 0; dof < EditorCurveBindingTransformRotationPropertyNames[mode].Length; dof++)
                                    {
                                        var tmpBinding = binding;
                                        tmpBinding.propertyName = EditorCurveBindingTransformRotationPropertyNames[mode][dof];
                                        RemoveEditorCurveCache(clip, tmpBinding);
                                    }
                                }
                            }
                            else if (IsTransformScaleCurveBinding(binding))
                            {
                                for (int dof = 0; dof < EditorCurveBindingTransformScalePropertyNames.Length; dof++)
                                {
                                    var tmpBinding = binding;
                                    tmpBinding.propertyName = EditorCurveBindingTransformScalePropertyNames[dof];
                                    RemoveEditorCurveCache(clip, tmpBinding);
                                }
                            }
                        }
                    }
                }
                else if (deleted == AnimationUtility.CurveModifiedType.ClipModified)
                {
                    ClearEditorCurveCache();
                    return;
                }
            }
            #endregion

            #region Ignore undo
            {
                System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
                if (stackTrace.FrameCount >= 2 && stackTrace.GetFrame(1).GetMethod().Name == "Internal_CallAnimationClipAwake")
                    return;
            }
            #endregion

            #region CheckConflictCurve
            if (deleted == AnimationUtility.CurveModifiedType.CurveModified)
            {
                if ((IsTransformPositionCurveBinding(binding) || IsTransformRotationCurveBinding(binding)))
                {
                    var boneIndex = GetBoneIndexFromCurveBinding(binding);
                    if (boneIndex >= 0)
                    {
                        if (isHuman && humanoidConflict[boneIndex])
                        {
                            EditorCommon.ShowNotification("Conflict");
                            Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the humanoid. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                            AnimationUtility.SetEditorCurve(clip, binding, null);
                            return;
                        }
                        else if (rootMotionBoneIndex >= 0 && boneIndex == 0)
                        {
                            EditorCommon.ShowNotification("Conflict");
                            Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                            AnimationUtility.SetEditorCurve(clip, binding, null);
                            return;
                        }
                        else if (!isHuman && rootMotionBoneIndex >= 0 && boneIndex == rootMotionBoneIndex)
                        {
                            EditorCommon.ShowNotification("Conflict");
                            Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Please edit RootT. '{0}'", editBones[boneIndex].name);
                            updateGenericRootMotion = true;
                        }
                    }
                }
            }
            #endregion
            /* Todo
            #region EditorOptions - clampMuscle
            if ((isHuman && clampMuscle) && deleted == AnimationUtility.CurveModifiedType.CurveModified && uAw.IsShowCurveEditor())
            {

            }
            #endregion
            */

            curvesWasModified[binding] = deleted;
        }

        #region EditorCurveBinding
        private void CreateEditorCurveBindingPropertyNames()
        {
            {
                EditorCurveBindingAnimatorIkTPropertyNames = new string[(int)AnimatorIKIndex.Total][];
                EditorCurveBindingAnimatorIkQPropertyNames = new string[(int)AnimatorIKIndex.Total][];
                for (int i = 0; i < (int)AnimatorIKIndex.Total; i++)
                {
                    EditorCurveBindingAnimatorIkTPropertyNames[i] = new string[3];
                    for (int dofIndex = 0; dofIndex < 3; dofIndex++)
                        EditorCurveBindingAnimatorIkTPropertyNames[i][dofIndex] = string.Format("{0}T{1}", (AnimatorIKIndex)i, DofIndex2String[dofIndex]);
                    EditorCurveBindingAnimatorIkQPropertyNames[i] = new string[4];
                    for (int dofIndex = 0; dofIndex < 4; dofIndex++)
                        EditorCurveBindingAnimatorIkQPropertyNames[i][dofIndex] = string.Format("{0}Q{1}", (AnimatorIKIndex)i, DofIndex2String[dofIndex]);
                }
            }
            {
                EditorCurveBindingAnimatorTDOFPropertyNames = new string[(int)AnimatorTDOFIndex.Total][];
                for (int i = 0; i < (int)AnimatorTDOFIndex.Total; i++)
                {
                    EditorCurveBindingAnimatorTDOFPropertyNames[i] = new string[3];
                    for (int dofIndex = 0; dofIndex < 3; dofIndex++)
                        EditorCurveBindingAnimatorTDOFPropertyNames[i][dofIndex] = string.Format("{0}TDOF{1}", AnimatorTDOFIndex2HumanBodyBones[i], DofIndex2String[dofIndex]);
                }
            }
            {
                EditorCurveBindingTransformRotationPropertyNames = new string[(int)URotationCurveInterpolation.Mode.Total][];
                for (int i = 0; i < (int) URotationCurveInterpolation.Mode.Total; i++)
                {
                    int dofCount;
                    switch ((URotationCurveInterpolation.Mode)i)
                    {
                    case URotationCurveInterpolation.Mode.RawQuaternions: dofCount = 4; break;
                    default: dofCount = 3; break;
                    }
                    EditorCurveBindingTransformRotationPropertyNames[i] = new string[dofCount];
                    for (int dofIndex = 0; dofIndex < dofCount; dofIndex++)
                        EditorCurveBindingTransformRotationPropertyNames[i][dofIndex] = URotationCurveInterpolation.PrefixForInterpolation[i] + DofIndex2String[dofIndex];
                }
            }
        }
        public readonly EditorCurveBinding[] AnimationCurveBindingAnimatorRootT =
        {
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootT.x"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootT.y"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootT.z"),
        };
        public readonly EditorCurveBinding[] AnimationCurveBindingAnimatorRootQ =
        {
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootQ.x"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootQ.y"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootQ.z"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "RootQ.w"),
        };
        public readonly EditorCurveBinding[] AnimationCurveBindingAnimatorMotionT =
        {
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionT.x"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionT.y"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionT.z"),
        };
        public readonly EditorCurveBinding[] AnimationCurveBindingAnimatorMotionQ =
        {
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionQ.x"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionQ.y"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionQ.z"),
            EditorCurveBinding.FloatCurve("", typeof(Animator), "MotionQ.w"),
        };
        private string[][] EditorCurveBindingAnimatorIkTPropertyNames;
        public EditorCurveBinding AnimationCurveBindingAnimatorIkT(AnimatorIKIndex ikIndex, int dofIndex)
        {
            return EditorCurveBinding.FloatCurve("", typeof(Animator), EditorCurveBindingAnimatorIkTPropertyNames[(int)ikIndex][dofIndex]);
        }
        private string[][] EditorCurveBindingAnimatorIkQPropertyNames;
        public EditorCurveBinding AnimationCurveBindingAnimatorIkQ(AnimatorIKIndex ikIndex, int dofIndex)
        {
            return EditorCurveBinding.FloatCurve("", typeof(Animator), EditorCurveBindingAnimatorIkQPropertyNames[(int)ikIndex][dofIndex]);
        }
        private string[][] EditorCurveBindingAnimatorTDOFPropertyNames;
        public EditorCurveBinding AnimationCurveBindingAnimatorTDOF(AnimatorTDOFIndex tdofIndex, int dofIndex)
        {
            return EditorCurveBinding.FloatCurve("", typeof(Animator), EditorCurveBindingAnimatorTDOFPropertyNames[(int)tdofIndex][dofIndex]);
        }
        public EditorCurveBinding AnimationCurveBindingAnimatorMuscle(int muscleIndex)
        {
            return EditorCurveBinding.FloatCurve("", typeof(Animator), musclePropertyName.PropertyNames[muscleIndex]);
        }
        public EditorCurveBinding AnimationCurveBindingAnimatorCustom(string propertyName)
        {
            return EditorCurveBinding.FloatCurve("", typeof(Animator), propertyName);
        }
        private static readonly string[] EditorCurveBindingTransformPositionPropertyNames =
        {
            "m_LocalPosition.x",
            "m_LocalPosition.y",
            "m_LocalPosition.z",
        };
        public EditorCurveBinding AnimationCurveBindingTransformPosition(int boneIndex, int dofIndex)
        {
            return EditorCurveBinding.FloatCurve(bonePaths[boneIndex], typeof(Transform), EditorCurveBindingTransformPositionPropertyNames[dofIndex]);
        }
        private string[][] EditorCurveBindingTransformRotationPropertyNames;
        public EditorCurveBinding AnimationCurveBindingTransformRotation(int boneIndex, int dofIndex, URotationCurveInterpolation.Mode mode)
        {
            return EditorCurveBinding.FloatCurve(bonePaths[boneIndex], typeof(Transform), EditorCurveBindingTransformRotationPropertyNames[(int)mode][dofIndex]);
        }
        private static readonly string[] EditorCurveBindingTransformScalePropertyNames =
        {
            "m_LocalScale.x",
            "m_LocalScale.y",
            "m_LocalScale.z",
        };
        public EditorCurveBinding AnimationCurveBindingTransformScale(int boneIndex, int dofIndex)
        {
            return EditorCurveBinding.FloatCurve(bonePaths[boneIndex], typeof(Transform), EditorCurveBindingTransformScalePropertyNames[dofIndex]);
        }
        public EditorCurveBinding AnimationCurveBindingBlendShape(SkinnedMeshRenderer renderer, string name)
        {
            return EditorCurveBinding.FloatCurve(AnimationUtility.CalculateTransformPath(renderer.transform, editGameObject.transform), typeof(SkinnedMeshRenderer), string.Format("blendShape.{0}", name));
        }

        public int GetBoneIndexFromCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type == typeof(Transform))
                return GetBoneIndexFromPath(binding.path);
            else
                return -1;
        }
        public int GetBoneIndexFromPath(string path)
        {
            int boneIndex;
            if (bonePathDic.TryGetValue(path, out boneIndex))
            {
                return boneIndex;
            }
            return -1;
        }
        public int GetRootTDofIndexFromCurveBinding(EditorCurveBinding binding)
        {
            for (int dofIndex = 0; dofIndex < 3; dofIndex++)
            {
                if (binding == AnimationCurveBindingAnimatorRootT[dofIndex])
                    return dofIndex;
            }
            return -1;
        }
        public int GetRootQDofIndexFromCurveBinding(EditorCurveBinding binding)
        {
            for (int dofIndex = 0; dofIndex < 4; dofIndex++)
            {
                if (binding == AnimationCurveBindingAnimatorRootQ[dofIndex])
                    return dofIndex;
            }
            return -1;
        }
        public int GetMuscleIndexFromCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Animator)) return -1;
            return GetMuscleIndexFromPropertyName(binding.propertyName);
        }
        public int GetMuscleIndexFromPropertyName(string propertyName)
        {
            int muscleIndex;
            if (musclePropertyName.PropertyNameDic.TryGetValue(propertyName, out muscleIndex))
            {
                return muscleIndex;
            }
            return -1;
        }
        public AnimatorIKIndex GetIkTIndexFromCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Animator)) return AnimatorIKIndex.None;
            for (var ikIndex = (AnimatorIKIndex)0; ikIndex < AnimatorIKIndex.Total; ikIndex++)
            {
                if (binding.propertyName.StartsWith(ikIndex.ToString() + "T"))
                    return ikIndex;
            }
            return AnimatorIKIndex.None;
        }
        public AnimatorIKIndex GetIkQIndexFromCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Animator)) return AnimatorIKIndex.None;
            for (var ikIndex = (AnimatorIKIndex)0; ikIndex < AnimatorIKIndex.Total; ikIndex++)
            {
                if (binding.propertyName.StartsWith(ikIndex.ToString() + "Q"))
                    return ikIndex;
            }
            return AnimatorIKIndex.None;
        }
        public AnimatorTDOFIndex GetTDOFIndexFromCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Animator)) return AnimatorTDOFIndex.None;
            var indexOf = binding.propertyName.IndexOf("TDOF.");
            if (indexOf < 0) return AnimatorTDOFIndex.None;
            var name = binding.propertyName.Remove(indexOf);
            for (var tdofIndex = (AnimatorTDOFIndex)0; tdofIndex < AnimatorTDOFIndex.Total; tdofIndex++)
            {
                if (name == tdofIndex.ToString())
                    return tdofIndex;
            }
            return AnimatorTDOFIndex.None;
        }
        public int GetDOFIndexFromCurveBinding(EditorCurveBinding binding)
        {
            for (int i = 0; i < DofIndex2String.Length; i++)
            {
                if (binding.propertyName.EndsWith(DofIndex2String[i]))
                    return i;
            }
            return -1;
        }
        public EditorCurveBinding? GetMirrorAnimationCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type == typeof(Animator))
            {
                int muscleIndex;
                AnimatorIKIndex ikTIndex, ikQIndex;
                AnimatorTDOFIndex tdofIndex;
                if (IsAnimatorRootCurveBinding(binding))
                {
                    return null;
                }
                else if ((muscleIndex = GetMuscleIndexFromCurveBinding(binding)) >= 0)
                {
                    var mmuscleIndex = GetMirrorMuscleIndex(muscleIndex);
                    if (mmuscleIndex < 0) return null;
                    return AnimationCurveBindingAnimatorMuscle(mmuscleIndex);
                }
                else if ((ikTIndex = GetIkTIndexFromCurveBinding(binding)) != AnimatorIKIndex.None)
                {
                    var mikTIndex = AnimatorIKMirrorIndexes[(int)ikTIndex];
                    if (mikTIndex < 0) return null;
                    var dofIndex = GetDOFIndexFromCurveBinding(binding);
                    if (dofIndex < 0) return null;
                    return AnimationCurveBindingAnimatorIkT(mikTIndex, dofIndex);
                }
                else if ((ikQIndex = GetIkQIndexFromCurveBinding(binding)) != AnimatorIKIndex.None)
                {
                    var mikQIndex = AnimatorIKMirrorIndexes[(int)ikQIndex];
                    if (mikQIndex < 0) return null;
                    var dofIndex = GetDOFIndexFromCurveBinding(binding);
                    if (dofIndex < 0) return null;
                    return AnimationCurveBindingAnimatorIkQ(mikQIndex, dofIndex);
                }
                else if ((tdofIndex = GetTDOFIndexFromCurveBinding(binding)) != AnimatorTDOFIndex.None)
                {
                    var mtdofIndex = AnimatorTDOFMirrorIndexes[(int)tdofIndex];
                    if (mtdofIndex < 0) return null;
                    var dofIndex = GetDOFIndexFromCurveBinding(binding);
                    if (dofIndex < 0) return null;
                    return AnimationCurveBindingAnimatorTDOF(mtdofIndex, dofIndex);
                }
                else
                {
                    return null;
                }
            }
            else if (binding.type == typeof(Transform))
            {
                var boneIndex = GetBoneIndexFromCurveBinding(binding);
                if (boneIndex < 0) return null;
                if (mirrorBoneIndexes[boneIndex] < 0) return null;
                binding.path = bonePaths[mirrorBoneIndexes[boneIndex]];
                return binding;
            }
            else
            {
                return null;
            }
        }

        public bool IsAnimatorRootCurveBinding(EditorCurveBinding binding)
        {
            return (GetRootTDofIndexFromCurveBinding(binding) >= 0 ||
                    GetRootQDofIndexFromCurveBinding(binding) >= 0);
        }
        public bool IsAnimatorReservedPropertyName(string propertyName)
        {
            for (int dof = 0; dof < 3; dof++)
            {
                if (propertyName == AnimationCurveBindingAnimatorRootT[dof].propertyName)
                    return true;
            }
            for (int dof = 0; dof < 4; dof++)
            {
                if (propertyName == AnimationCurveBindingAnimatorRootQ[dof].propertyName)
                    return true;
            }
            for (int dof = 0; dof < 3; dof++)
            {
                if (propertyName == AnimationCurveBindingAnimatorMotionT[dof].propertyName)
                    return true;
            }
            for (int dof = 0; dof < 4; dof++)
            {
                if (propertyName == AnimationCurveBindingAnimatorMotionQ[dof].propertyName)
                    return true;
            }
            for (var i = 0; i < (int)AnimatorIKIndex.Total; i++)
            {
                for (int dof = 0; dof < 3; dof++)
                {
                    if (propertyName == EditorCurveBindingAnimatorIkTPropertyNames[i][dof])
                        return true;
                }
                for (int dof = 0; dof < 4; dof++)
                {
                    if (propertyName == EditorCurveBindingAnimatorIkQPropertyNames[i][dof])
                        return true;
                }
            }
            for (var i = 0; i < (int)AnimatorTDOFIndex.Total; i++)
            {
                for (int dof = 0; dof < 3; dof++)
                {
                    if (propertyName == EditorCurveBindingAnimatorTDOFPropertyNames[i][dof])
                        return true;
                }
            }
            if (GetMuscleIndexFromPropertyName(propertyName) >= 0)
                return true;

            return false;
        }
        public bool IsTransformPositionCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Transform)) return false;
            return binding.propertyName.StartsWith("m_LocalPosition.");
        }
        public bool IsTransformRotationCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Transform)) return false;
            for (int i = 0; i < URotationCurveInterpolation.PrefixForInterpolation.Length; i++)
            {
                if (URotationCurveInterpolation.PrefixForInterpolation[i] == null) continue;
                if (binding.propertyName.StartsWith(URotationCurveInterpolation.PrefixForInterpolation[i]))
                    return true;
            }
            return false;
        }
        public bool IsTransformScaleCurveBinding(EditorCurveBinding binding)
        {
            if (binding.type != typeof(Transform)) return false;
            return binding.propertyName.StartsWith("m_LocalScale.");
        }

        #endregion

        #region HumanPose
        public void GetHumanPose(ref HumanPose humanPose)
        {
            if (!isHuman || editHumanPoseHandler == null) return;
            var t = editGameObject.transform;
            TransformPoseSave.SaveData save = new TransformPoseSave.SaveData(t);
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
            editHumanPoseHandler.GetHumanPose(ref humanPose);
            save.LoadLocal(t);
        }
        public void GetHumanPoseCurve(ref HumanPose humanPose, float time = -1f)
        {
            humanPose.bodyPosition = GetAnimationCurveAnimatorRootT(time);
            humanPose.bodyRotation = GetAnimationCurveAnimatorRootQ(time);
            humanPose.muscles = new float[HumanTrait.MuscleCount];
            for (int i = 0; i < humanPose.muscles.Length; i++)
            {
                humanPose.muscles[i] = GetAnimationCurveAnimatorMuscle(i, time);
            }
        }
        #endregion

        #region EditorCurveCache
        #region EditorCurveBindingCache
        private Dictionary<string, Dictionary<Type, Dictionary<string, int>>> editorCurveBindingHashCacheDic;
        private void ClearEditorCurveBindingHashCode()
        {
            if (editorCurveBindingHashCacheDic == null)
                editorCurveBindingHashCacheDic = new Dictionary<string, Dictionary<Type, Dictionary<string, int>>>();
            else
                editorCurveBindingHashCacheDic.Clear();
        }
        public int GetEditorCurveBindingHashCode(EditorCurveBinding binding)
        {
            if (editorCurveBindingHashCacheDic == null)
                editorCurveBindingHashCacheDic = new Dictionary<string, Dictionary<Type, Dictionary<string, int>>>();

            int hashCode;
            Dictionary<Type, Dictionary<string, int>> typeNameDic;
            if (editorCurveBindingHashCacheDic.TryGetValue(binding.path, out typeNameDic))
            {
                Dictionary<string, int> propertyNameDic;
                if (typeNameDic.TryGetValue(binding.type, out propertyNameDic))
                {
                    if (propertyNameDic.TryGetValue(binding.propertyName, out hashCode))
                    {
                        return hashCode;
                    }
                    else
                    {
                        hashCode = binding.GetHashCode();
                        propertyNameDic.Add(binding.propertyName, hashCode);
                    }
                }
                else
                {
                    propertyNameDic = new Dictionary<string, int>();
                    hashCode = binding.GetHashCode();
                    propertyNameDic.Add(binding.propertyName, hashCode);
                    typeNameDic.Add(binding.type, propertyNameDic);
                }
            }
            else
            {
                typeNameDic = new Dictionary<Type, Dictionary<string, int>>();
                hashCode = binding.GetHashCode();
                {
                    var propertyNameDic = new Dictionary<string, int>();
                    propertyNameDic.Add(binding.propertyName, hashCode);
                    typeNameDic.Add(binding.type, propertyNameDic);
                }
                editorCurveBindingHashCacheDic.Add(binding.path, typeNameDic);
            }
            
            return hashCode;
        }
        #endregion

        private AnimationClip editorCurveCacheClip;
        private Dictionary<int, AnimationCurve> editorCurveCacheDic;

        private void ClearEditorCurveCache()
        {
            ClearEditorCurveBindingHashCode();

            editorCurveCacheClip = null;
            if (editorCurveCacheDic == null)
                editorCurveCacheDic = new Dictionary<int, AnimationCurve>();
            else
                editorCurveCacheDic.Clear();
        }
        private void CheckChangeClipClearEditorCurveCache(AnimationClip clip)
        {
            if (clip != editorCurveCacheClip)
            {
                ClearEditorCurveCache();
                editorCurveCacheClip = clip;
            }
        }
        private AnimationCurve GetEditorCurveCache(AnimationClip clip, EditorCurveBinding binding)
        {
            CheckChangeClipClearEditorCurveCache(clip);
            var hash = GetEditorCurveBindingHashCode(binding);
            AnimationCurve curve = null;
            if (!editorCurveCacheDic.TryGetValue(hash, out curve))
            {
                curve = AnimationUtility.GetEditorCurve(clip, binding);
                editorCurveCacheDic[hash] = curve;
            }
            return curve;
        }
        private void SetEditorCurveCache(AnimationClip clip, EditorCurveBinding binding, AnimationCurve curve)
        {
            CheckChangeClipClearEditorCurveCache(clip);
            clip.SetCurve(binding.path, binding.type, binding.propertyName, curve);
            uAw.CurveWasModified(clip, binding, curve != null ? AnimationUtility.CurveModifiedType.CurveModified : AnimationUtility.CurveModifiedType.CurveDeleted);
            var hash = GetEditorCurveBindingHashCode(binding);
            if (editorCurveCacheDic == null)
                editorCurveCacheDic = new Dictionary<int, AnimationCurve>();
            editorCurveCacheDic[hash] = curve;
        }
        private void RemoveEditorCurveCache(AnimationClip clip, EditorCurveBinding binding)
        {
            CheckChangeClipClearEditorCurveCache(clip);
            if (editorCurveCacheDic == null) return;
            var hash = GetEditorCurveBindingHashCode(binding);
            editorCurveCacheDic.Remove(hash);
        }
        #endregion

        #region PoseTemplate
        public void SavePoseTemplate(PoseTemplate poseTemplate)
        {
            poseTemplate.Reset();
            poseTemplate.isHuman = isHuman;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            var time = uAw.GetCurrentTime();
            #region Human
            if (isHuman)
            {
                poseTemplate.haveRootT = true;
                poseTemplate.rootT = GetAnimationCurveAnimatorRootT(time);
                poseTemplate.haveRootQ = true;
                poseTemplate.rootQ = GetAnimationCurveAnimatorRootQ(time);
                {
                    Dictionary<string, float> muscleList = new Dictionary<string, float>();
                    for (int muscleIndex = 0; muscleIndex < musclePropertyName.PropertyNames.Length; muscleIndex++)
                        muscleList.Add(musclePropertyName.PropertyNames[muscleIndex], GetAnimationCurveAnimatorMuscle(muscleIndex, time));
                    poseTemplate.musclePropertyNames = muscleList.Keys.ToArray();
                    poseTemplate.muscleValues = muscleList.Values.ToArray();
                }
                {
                    Dictionary<AnimatorTDOFIndex, Vector3> tdofIndices = new Dictionary<AnimatorTDOFIndex, Vector3>();
                    for (AnimatorTDOFIndex tdofIndex = 0; tdofIndex < AnimatorTDOFIndex.Total; tdofIndex++)
                        tdofIndices.Add(tdofIndex, GetAnimationCurveAnimatorTDOF(tdofIndex, time));
                    poseTemplate.tdofIndices = tdofIndices.Keys.ToArray();
                    poseTemplate.tdofValues = tdofIndices.Values.ToArray();
                }
                {
                    Dictionary<AnimatorIKIndex, PoseTemplate.IKData> ikIndices = new Dictionary<AnimatorIKIndex, PoseTemplate.IKData>();
                    for (AnimatorIKIndex ikIndex = 0; ikIndex < AnimatorIKIndex.Total; ikIndex++)
                    {
                        ikIndices.Add(ikIndex, new PoseTemplate.IKData()
                        {
                            position = GetAnimationCurveAnimatorIkT(ikIndex, time),
                            rotation = GetAnimationCurveAnimatorIkQ(ikIndex, time),
                        });
                    }
                    poseTemplate.ikIndices = ikIndices.Keys.ToArray();
                    poseTemplate.ikValues = ikIndices.Values.ToArray();
                }
            }
            #endregion
            #region Generic
            {
                if (!isHuman && rootMotionBoneIndex >= 0)
                {
                    poseTemplate.haveRootT = true;
                    poseTemplate.rootT = GetAnimationCurveAnimatorRootT(time);
                    poseTemplate.haveRootQ = true;
                    poseTemplate.rootQ = GetAnimationCurveAnimatorRootQ(time);
                }
                Dictionary<string, PoseTemplate.TransformData> transformList = new Dictionary<string, PoseTemplate.TransformData>();
                {   //Root
                    var boneIndex = 0;
                    var t = editBones[boneIndex].transform;
                    transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                    {
                        position = t.localPosition - transformPoseSave.startPosition,
                        rotation = Quaternion.Inverse(transformPoseSave.startRotation) * t.localRotation,
                        scale = t.localScale - transformPoseSave.startScale,
                    });
                }
                for (int boneIndex = 1; boneIndex < bones.Length; boneIndex++)
                {
                    if (isHuman && humanoidConflict[boneIndex])
                    {
                        var t = editBones[boneIndex].transform;
                        transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                        {
                            position = t.localPosition,
                            rotation = t.localRotation,
                            scale = t.localScale,
                        });
                    }
                    else
                    {
                        transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                        {
                            position = GetAnimationCurveTransformPosition(boneIndex, time),
                            rotation = GetAnimationCurveTransformRotation(boneIndex, time),
                            scale = GetAnimationCurveTransformScale(boneIndex, time),
                        });
                    }
                }
                poseTemplate.transformPaths = transformList.Keys.ToArray();
                poseTemplate.transformValues = transformList.Values.ToArray();
            }
            #endregion
            #region BlendShape
            {
                Dictionary<string, PoseTemplate.BlendShapeData> blendShapeList = new Dictionary<string, PoseTemplate.BlendShapeData>();
                foreach (var renderer in editGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    if (renderer == null || renderer.sharedMesh == null || renderer.sharedMesh.blendShapeCount <= 0) continue;
                    var path = AnimationUtility.CalculateTransformPath(renderer.transform, editGameObject.transform);
                    var data = new PoseTemplate.BlendShapeData()
                    {
                        names = new string[renderer.sharedMesh.blendShapeCount],
                        weights = new float[renderer.sharedMesh.blendShapeCount],
                    };
                    for (int i = 0; i < renderer.sharedMesh.blendShapeCount; i++)
                    {
                        data.names[i] = renderer.sharedMesh.GetBlendShapeName(i);
                        data.weights[i] = GetAnimationCurveBlendShape(renderer, data.names[i], time);
                    }
                    blendShapeList.Add(path, data);
                }
                poseTemplate.blendShapePaths = blendShapeList.Keys.ToArray();
                poseTemplate.blendShapeValues = blendShapeList.Values.ToArray();
            }
            #endregion
        }
        public void SaveSelectionPoseTemplate(PoseTemplate poseTemplate)
        {
            bool selectRoot = SelectionGameObjectsIndexOf(vaw.gameObject) >= 0;
            var selectHumanoidIndexes = SelectionGameObjectsHumanoidIndex();
            var selectMuscleIndexes = SelectionGameObjectsMuscleIndex();
            var selectAnimatorIKTargetsHumanoidIndexes = animatorIK.SelectionAnimatorIKTargetsHumanoidIndexes();
            var selectOriginalIKTargetsBoneIndexes = originalIK.SelectionOriginalIKTargetsBoneIndexes();
            //
            poseTemplate.Reset();
            poseTemplate.isHuman = isHuman;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            var time = uAw.GetCurrentTime();
            #region Human
            if (isHuman)
            {
                if (selectRoot)
                {
                    poseTemplate.haveRootT = true;
                    poseTemplate.rootT = GetAnimationCurveAnimatorRootT(time);
                    poseTemplate.haveRootQ = true;
                    poseTemplate.rootQ = GetAnimationCurveAnimatorRootQ(time);
                }
                {
                    Dictionary<string, float> muscleList = new Dictionary<string, float>();
                    for (int muscleIndex = 0; muscleIndex < musclePropertyName.PropertyNames.Length; muscleIndex++)
                    {
                        if (EditorCommon.ArrayContains(selectMuscleIndexes, muscleIndex) ||
                            EditorCommon.ArrayContains(selectAnimatorIKTargetsHumanoidIndexes, (HumanBodyBones)HumanTrait.BoneFromMuscle(muscleIndex)))
                        {
                            muscleList.Add(musclePropertyName.PropertyNames[muscleIndex], GetAnimationCurveAnimatorMuscle(muscleIndex, time));
                        }
                    }
                    poseTemplate.musclePropertyNames = muscleList.Keys.ToArray();
                    poseTemplate.muscleValues = muscleList.Values.ToArray();
                }
                {
                    Dictionary<AnimatorTDOFIndex, Vector3> tdofIndices = new Dictionary<AnimatorTDOFIndex, Vector3>();
                    for (AnimatorTDOFIndex tdofIndex = 0; tdofIndex < AnimatorTDOFIndex.Total; tdofIndex++)
                    {
                        if (EditorCommon.ArrayContains(selectHumanoidIndexes, AnimatorTDOFIndex2HumanBodyBones[(int)tdofIndex]) ||
                            EditorCommon.ArrayContains(selectAnimatorIKTargetsHumanoidIndexes, AnimatorTDOFIndex2HumanBodyBones[(int)tdofIndex]))
                        {
                            tdofIndices.Add(tdofIndex, GetAnimationCurveAnimatorTDOF(tdofIndex, time));
                        }
                    }
                    poseTemplate.tdofIndices = tdofIndices.Keys.ToArray();
                    poseTemplate.tdofValues = tdofIndices.Values.ToArray();
                }
                {
                    Dictionary<AnimatorIKIndex, PoseTemplate.IKData> ikIndices = new Dictionary<AnimatorIKIndex, PoseTemplate.IKData>();
                    for (AnimatorIKIndex ikIndex = 0; ikIndex < AnimatorIKIndex.Total; ikIndex++)
                    {
                        if (EditorCommon.ArrayContains(selectHumanoidIndexes, AnimatorIKIndex2HumanBodyBones[(int)ikIndex]) ||
                            EditorCommon.ArrayContains(selectAnimatorIKTargetsHumanoidIndexes, AnimatorIKIndex2HumanBodyBones[(int)ikIndex]))
                        {
                            ikIndices.Add(ikIndex, new PoseTemplate.IKData()
                            {
                                position = GetAnimationCurveAnimatorIkT(ikIndex, time),
                                rotation = GetAnimationCurveAnimatorIkQ(ikIndex, time),
                            });
                        }
                    }
                    poseTemplate.ikIndices = ikIndices.Keys.ToArray();
                    poseTemplate.ikValues = ikIndices.Values.ToArray();
                }
            }
            #endregion
            #region Generic
            {
                if (!isHuman && rootMotionBoneIndex >= 0 && selectionBones.Contains(rootMotionBoneIndex))
                {
                    poseTemplate.haveRootT = true;
                    poseTemplate.rootT = GetAnimationCurveAnimatorRootT(time);
                    poseTemplate.haveRootQ = true;
                    poseTemplate.rootQ = GetAnimationCurveAnimatorRootQ(time);
                }
                Dictionary<string, PoseTemplate.TransformData> transformList = new Dictionary<string, PoseTemplate.TransformData>();
                {   //Root
                    var boneIndex = 0;
                    if (selectionBones.Contains(boneIndex) ||
                        EditorCommon.ArrayContains(selectOriginalIKTargetsBoneIndexes, boneIndex))
                    {
                        var t = editBones[boneIndex].transform;
                        transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                        {
                            position = t.localPosition - transformPoseSave.startPosition,
                            rotation = Quaternion.Inverse(transformPoseSave.startRotation) * t.localRotation,
                            scale = t.localScale - transformPoseSave.startScale,
                        });
                    }
                }
                for (int boneIndex = 1; boneIndex < bones.Length; boneIndex++)
                {
                    if (selectionBones.Contains(boneIndex) ||
                        EditorCommon.ArrayContains(selectOriginalIKTargetsBoneIndexes, boneIndex))
                    {
                        if (isHuman && humanoidConflict[boneIndex])
                        {
                            var t = editBones[boneIndex].transform;
                            transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                            {
                                position = t.localPosition,
                                rotation = t.localRotation,
                                scale = t.localScale,
                            });
                        }
                        else
                        {
                            transformList.Add(bonePaths[boneIndex], new PoseTemplate.TransformData()
                            {
                                position = GetAnimationCurveTransformPosition(boneIndex, time),
                                rotation = GetAnimationCurveTransformRotation(boneIndex, time),
                                scale = GetAnimationCurveTransformScale(boneIndex, time),
                            });
                        }
                    }
                }
                poseTemplate.transformPaths = transformList.Keys.ToArray();
                poseTemplate.transformValues = transformList.Values.ToArray();
            }
            #endregion
            #region BlendShape
            foreach (var boneIndex in selectionBones)
            {
                Dictionary<string, PoseTemplate.BlendShapeData> blendShapeList = new Dictionary<string, PoseTemplate.BlendShapeData>();
                foreach (var renderer in editBones[boneIndex].GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    if (renderer == null || renderer.sharedMesh == null || renderer.sharedMesh.blendShapeCount <= 0) continue;
                    var path = AnimationUtility.CalculateTransformPath(renderer.transform, editGameObject.transform);
                    var data = new PoseTemplate.BlendShapeData()
                    {
                        names = new string[renderer.sharedMesh.blendShapeCount],
                        weights = new float[renderer.sharedMesh.blendShapeCount],
                    };
                    for (int i = 0; i < renderer.sharedMesh.blendShapeCount; i++)
                    {
                        data.names[i] = renderer.sharedMesh.GetBlendShapeName(i);
                        data.weights[i] = GetAnimationCurveBlendShape(renderer, data.names[i], time);
                    }
                    blendShapeList.Add(path, data);
                }
                poseTemplate.blendShapePaths = blendShapeList.Keys.ToArray();
                poseTemplate.blendShapeValues = blendShapeList.Values.ToArray();
            }
            #endregion
        }
        public void LoadPoseTemplate(PoseTemplate poseTemplate, bool updateAll = false)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            var time = uAw.GetCurrentTime();
            var mirrorSet = true;
            if (updateAll)
            {
                ResetAllHaveAnimationCurve();
                mirrorSet = false;
            }
            #region Human
            if (isHuman && poseTemplate.isHuman)
            {
                if (poseTemplate.haveRootT)
                {
                    SetAnimationCurveAnimatorRootT(poseTemplate.rootT, time);
                }
                if (poseTemplate.haveRootQ)
                {
                    SetAnimationCurveAnimatorRootQ(poseTemplate.rootQ, time);
                }
                if (poseTemplate.musclePropertyNames != null && poseTemplate.muscleValues != null)
                {
                    Assert.IsTrue(poseTemplate.musclePropertyNames.Length == poseTemplate.muscleValues.Length);
                    for (int i = 0; i < poseTemplate.musclePropertyNames.Length; i++)
                    {
                        var muscleIndex = GetMuscleIndexFromPropertyName(poseTemplate.musclePropertyNames[i]);
                        if (muscleIndex < 0) continue;
                        var value = poseTemplate.muscleValues[i];
                        if (!IsHaveAnimationCurveAnimatorMuscle(muscleIndex) && value == 0f) continue;
                        SetAnimationCurveAnimatorMuscle(muscleIndex, poseTemplate.muscleValues[i], mirrorSet, time);
                    }
                }
                if (poseTemplate.tdofIndices != null && poseTemplate.tdofValues != null)
                {
                    Assert.IsTrue(poseTemplate.tdofIndices.Length == poseTemplate.tdofValues.Length);
                    for (int i = 0; i < poseTemplate.tdofIndices.Length; i++)
                    {
                        var tdofIndex = poseTemplate.tdofIndices[i];
                        var value = poseTemplate.tdofValues[i];
                        if (!IsHaveAnimationCurveAnimatorTDOF(tdofIndex) && value == Vector3.zero) continue;
                        SetAnimationCurveAnimatorTDOF(tdofIndex, value, mirrorSet, time);
                    }
                }
                if (poseTemplate.ikIndices != null && poseTemplate.ikValues != null)
                {
                    Assert.IsTrue(poseTemplate.ikIndices.Length == poseTemplate.ikValues.Length);
                    for (int i = 0; i < poseTemplate.ikIndices.Length; i++)
                    {
                        var ikIndex = poseTemplate.ikIndices[i];
                        var value = poseTemplate.ikValues[i];
                        if (IsHaveAnimationCurveAnimatorIkT(ikIndex) || value.position != Vector3.zero)
                        {
                            SetAnimationCurveAnimatorIkT(ikIndex, value.position, time);
                        }
                        if (IsHaveAnimationCurveAnimatorIkQ(ikIndex) || value.rotation != Quaternion.identity)
                        {
                            SetAnimationCurveAnimatorIkQ(ikIndex, value.rotation, time);
                        }
                    }
                }
                if (poseTemplate.haveRootT || poseTemplate.haveRootQ)
                {
                    DisableAnimatorRootCorrection();
                }
            }
            #endregion
            #region Generic
            if (!isHuman)
            {
                if (rootMotionBoneIndex >= 0)
                {
                    if (poseTemplate.haveRootT)
                    {
                        SetAnimationCurveAnimatorRootT(poseTemplate.rootT, time);
                    }
                    if (poseTemplate.haveRootQ)
                    {
                        SetAnimationCurveAnimatorRootQ(poseTemplate.rootQ, time);
                    }
                }
            }
            if (poseTemplate.transformPaths != null && poseTemplate.transformValues != null)
            {
                Assert.IsTrue(poseTemplate.transformPaths.Length == poseTemplate.transformValues.Length);
                for (int i = 0; i < poseTemplate.transformPaths.Length; i++)
                {
                    var boneIndex = GetBoneIndexFromPath(poseTemplate.transformPaths[i]);
                    if (boneIndex < 0 || (isHuman && humanoidConflict[boneIndex])) continue;
                    var position = poseTemplate.transformValues[i].position;
                    var rotation = poseTemplate.transformValues[i].rotation;
                    var scale = poseTemplate.transformValues[i].scale;
                    if (boneIndex == 0)
                    {   //Root
                        if (rootMotionBoneIndex >= 0) continue;
                        position = transformPoseSave.startPosition + position;
                        rotation = transformPoseSave.startRotation * rotation;
                        scale = transformPoseSave.startScale + scale;
                    }
                    else if (rootMotionBoneIndex >= 0 && boneIndex == rootMotionBoneIndex)
                    {
                        updateGenericRootMotion = true;
                        continue;
                    }
                    if (IsHaveAnimationCurveTransformPosition(boneIndex) || position != boneSaveTransforms[boneIndex].localPosition)
                        SetAnimationCurveTransformPosition(boneIndex, position, mirrorSet, time);
                    if (IsHaveAnimationCurveTransformRotation(boneIndex) != URotationCurveInterpolation.Mode.Undefined || rotation != boneSaveTransforms[boneIndex].localRotation)
                        SetAnimationCurveTransformRotation(boneIndex, rotation, mirrorSet, time);
                    if (IsHaveAnimationCurveTransformScale(boneIndex) || scale != boneSaveTransforms[boneIndex].localScale)
                        SetAnimationCurveTransformScale(boneIndex, scale, mirrorSet, time);
                }
            }
            #endregion
            #region BlendShape
            if (poseTemplate.blendShapePaths != null && poseTemplate.blendShapeValues != null)
            {
                foreach (var renderer in editGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
                {
                    if (renderer == null || renderer.sharedMesh == null || renderer.sharedMesh.blendShapeCount <= 0) continue;
                    var path = AnimationUtility.CalculateTransformPath(renderer.transform, editGameObject.transform);
                    var index = EditorCommon.ArrayIndexOf(poseTemplate.blendShapePaths, path);
                    if (index < 0) continue;
                    for (int i = 0; i < poseTemplate.blendShapeValues[index].names.Length; i++)
                    {
                        if (IsHaveAnimationCurveBlendShape(renderer, poseTemplate.blendShapeValues[index].names[i]) || poseTemplate.blendShapeValues[index].weights[i] != blendShapeWeightSave.GetOriginalWeight(renderer, poseTemplate.blendShapeValues[index].names[i]))
                            SetAnimationCurveBlendShape(renderer, poseTemplate.blendShapeValues[index].names[i], poseTemplate.blendShapeValues[index].weights[i], mirrorSet, time);
                    }
                }
            }
            #endregion
            SetUpdateResampleAnimation();
            SetSynchroIKtargetAll(true);
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
        }
        #endregion

        #region IK
        public void IKHandleGUI()
        {
            animatorIK.HandleGUI();
            originalIK.HandleGUI();
        }
        public void IKTargetGUI()
        {
            animatorIK.TargetGUI();
            originalIK.TargetGUI();
        }

        private void IKUpdateBones()
        {
            animatorIK.UpdateBones();
        }
        private void IKChangeSelection()
        {
            if (animatorIK.ChangeSelectionIK()) return;
            if (originalIK.ChangeSelectionIK()) return;
        }

        public void ClearIkTargetSelect()
        {
            animatorIK.ikTargetSelect = null;
            animatorIK.OnSelectionChange();
            originalIK.ikTargetSelect = null;
            originalIK.OnSelectionChange();
        }

        public void SetUpdateSelectionIKtarget()
        {
            if (animatorIK.ikTargetSelect != null)
            {
                foreach (var ikTarget in animatorIK.ikTargetSelect)
                {
                    animatorIK.SetUpdateIKtargetAnimatorIK(ikTarget);
                }
            }
            if (originalIK.ikTargetSelect != null)
            {
                foreach (var ikTarget in originalIK.ikTargetSelect)
                {
                    originalIK.SetUpdateIKtargetOriginalIK(ikTarget);
                }
            }
        }

        public bool IsIKBone(HumanBodyBones humanoidIndex)
        {
            return animatorIK.IsIKBone(humanoidIndex) != AnimatorIKCore.IKTarget.None ||
                    originalIK.IsIKBone(humanoidIndex) >= 0;
        }
        public bool IsIKBone(int boneIndex)
        {
            return animatorIK.IsIKBone(boneIndex2humanoidIndex[boneIndex]) != AnimatorIKCore.IKTarget.None ||
                    originalIK.IsIKBone(boneIndex) >= 0;
        }

        public void SetUpdateIKtargetBone(int boneIndex)
        {
            if (boneIndex < 0) return;
            originalIK.SetUpdateIKtargetBone(boneIndex);
        }
        public void SetUpdateIKtargetMuscle(int muscleIndex)
        {
            if (muscleIndex < 0) return;
            animatorIK.SetUpdateIKtargetMuscle(muscleIndex);
            originalIK.SetUpdateIKtargetMuscle(muscleIndex);
        }
        public void SetUpdateIKtargetHumanoidIndex(HumanBodyBones humanoidIndex)
        {
            if (humanoidIndex < 0) return;
            animatorIK.SetUpdateIKtargetHumanoidIndex(humanoidIndex);
            originalIK.SetUpdateIKtargetHumanoidIndex(humanoidIndex);
        }
        public void SetUpdateIKtargetTdofIndex(AnimatorTDOFIndex tdofIndex)
        {
            if (tdofIndex < 0) return;
            animatorIK.SetUpdateIKtargetTdofIndex(tdofIndex);
            originalIK.SetUpdateIKtargetTdofIndex(tdofIndex);
        }
        public void SetUpdateIKtargetAnimatorIK(AnimatorIKCore.IKTarget ikTarget)
        {
            if (ikTarget < 0) return;
            animatorIK.SetUpdateIKtargetAnimatorIK(ikTarget);
            for (int humanoidIndex = 0; humanoidIndex < AnimatorIKCore.HumanBonesUpdateAnimatorIK.Length; humanoidIndex++)
            {
                if (AnimatorIKCore.HumanBonesUpdateAnimatorIK[humanoidIndex] == ikTarget)
                {
                    originalIK.SetUpdateIKtargetBone(humanoidIndex2boneIndex[humanoidIndex]);
                }
            }
        }
        public void SetUpdateIKtargetOriginalIK(int ikTarget)
        {
            if (ikTarget < 0) return;
            originalIK.SetUpdateIKtargetOriginalIK(ikTarget);
        }
        public void SetUpdateIKtargetAll(bool flag)
        {
            animatorIK.SetUpdateIKtargetAll(flag);
            originalIK.SetUpdateIKtargetAll(flag);
        }
        public bool GetUpdateIKtargetAll()
        {
            return animatorIK.GetUpdateIKtargetAll() || originalIK.GetUpdateIKtargetAll();
        }

        public void SetSynchroIKtargetAll(bool flag)
        {
            animatorIK.SetSynchroIKtargetAll(flag);
            originalIK.SetSynchroIKtargetAll(flag);
        }
        public void UpdateSynchroIKSet()
        {
            animatorIK.UpdateSynchroIKSet();
            originalIK.UpdateSynchroIKSet();
        }
        #endregion

        #region AnimationCurve
        private bool beginChangeAnimationCurve;
        private void BeginChangeAnimationCurve()
        {
            if (beginChangeAnimationCurve) return;
            uAnimatorControllerTool.SetAnimatorController(null);
            uParameterControllerEditor.SetAnimatorController(null);
            uAw.ClearKeySelections();
            beginChangeAnimationCurve = true;
        }
        private void EndChangeAnimationCurve()
        {
            if (!beginChangeAnimationCurve) return;
            if (vaw.animator != null)
            {
                var ac = GetAnimatorController();
                uAnimatorControllerTool.SetAnimatorController(ac);
                uParameterControllerEditor.SetAnimatorController(ac);
            }
            beginChangeAnimationCurve = false;
        }

        public void SetPoseHumanoidDefault()
        {
            ResetAllHaveAnimationCurve();
            SetUpdateResampleAnimation();
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
            SetSynchroIKtargetAll(true);
            DisableAnimatorRootCorrection();
        }
        public void SetPoseEditStart()
        {
            ResetAllHaveAnimationCurve();
            SetUpdateResampleAnimation();
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
            SetSynchroIKtargetAll(true);
            DisableAnimatorRootCorrection();
        }
        public void SetPoseBind()
        {
            ResetAllHaveAnimationCurve();
            transformPoseSave.ResetBindTransform();
            SetAllChangedAnimationCurve();
            SetUpdateResampleAnimation();
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
            SetSynchroIKtargetAll(true);
            DisableAnimatorRootCorrection();
        }
        public void SetPosePrefab()
        {
            var prefab = PrefabUtility.GetPrefabParent(vaw.gameObject) as GameObject;
            if (prefab == null) return;

            ResetAllHaveAnimationCurve();
            transformPoseSave.ResetPrefabTransform();
            blendShapeWeightSave.ResetPrefabWeight();
            SetAllChangedAnimationCurve();
            SetUpdateResampleAnimation();
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
            SetSynchroIKtargetAll(true);
            DisableAnimatorRootCorrection();
        }
        public void SetPoseMirror()
        {
            {
                var rootT = GetAnimationCurveAnimatorRootT();
                if (IsHaveAnimationCurveAnimatorRootT())
                    SetAnimationCurveAnimatorRootT(new Vector3(-rootT.x, rootT.y, rootT.z));
                var rootQ = GetAnimationCurveAnimatorRootQ();
                if (IsHaveAnimationCurveAnimatorRootQ())
                    SetAnimationCurveAnimatorRootQ(new Quaternion(rootQ.x, -rootQ.y, -rootQ.z, rootQ.w));
            }
            {
                var values = new float[HumanTrait.MuscleCount];
                for (int i = 0; i < values.Length; i++)
                {
                    var mmi = GetMirrorMuscleIndex(i);
                    if (mmi < 0)
                        values[i] = float.MaxValue;
                    else
                        values[i] = GetAnimationCurveAnimatorMuscle(mmi);
                }
                for (int i = 0; i < values.Length; i++)
                {
                    if (values[i] == float.MaxValue)
                    {
                        var hi = HumanTrait.BoneFromMuscle(i);
                        if (i == HumanTrait.MuscleFromBone(hi, 0) || i == HumanTrait.MuscleFromBone(hi, 1))
                        {
                            if (!IsHaveAnimationCurveAnimatorMuscle(i)) continue;
                            SetAnimationCurveAnimatorMuscle(i, -GetAnimationCurveAnimatorMuscle(i), false);
                        }
                    }
                    else
                    {
                        var mmi = GetMirrorMuscleIndex(i);
                        if (!IsHaveAnimationCurveAnimatorMuscle(i) && !IsHaveAnimationCurveAnimatorMuscle(mmi)) continue;
                        SetAnimationCurveAnimatorMuscle(i, values[i], false);
                    }
                }
            }
            {
                Vector3[] saves = new Vector3[(int)AnimatorTDOFIndex.Total];
                for (var tdof = (AnimatorTDOFIndex)0; tdof < AnimatorTDOFIndex.Total; tdof++)
                {
                    saves[(int)tdof] = GetAnimationCurveAnimatorTDOF(tdof);
                }
                for (var tdof = (AnimatorTDOFIndex)0; tdof < AnimatorTDOFIndex.Total; tdof++)
                {
                    if (!IsHaveAnimationCurveAnimatorTDOF(tdof)) continue;
                    var mmi = AnimatorTDOFMirrorIndexes[(int)tdof];
                    var vec = Vector3.zero;
                    if (mmi != AnimatorTDOFIndex.None)
                    {
                        vec = Vector3.Scale(saves[(int)mmi], HumanBonesAnimatorTDOFIndex[(int)AnimatorTDOFIndex2HumanBodyBones[(int)mmi]].mirror);
                    }
                    else
                    {
                        vec = saves[(int)tdof];
                        vec.z = -vec.z;
                    }
                    SetAnimationCurveAnimatorTDOF(tdof, vec, false);
                }
            }
            {
                var values = new Dictionary<int, TransformPoseSave.SaveData>();
                foreach (var binding in AnimationUtility.GetCurveBindings(uAw.GetSelectionAnimationClip()))
                {
                    var boneIndex = GetBoneIndexFromCurveBinding(binding);
                    if (boneIndex < 0) continue;
                    if (values.ContainsKey(boneIndex)) continue;
                    values.Add(boneIndex, new TransformPoseSave.SaveData());
                    var mbi = mirrorBoneIndexes[boneIndex];
                    if (mbi >= 0)
                    {
                        var mt = editBones[mbi].transform;
                        values[boneIndex].localPosition = GetMirrorBoneLocalPosition(mbi, mt.localPosition);
                        values[boneIndex].localRotation = GetMirrorBoneLocalRotation(mbi, mt.localRotation);
                        values[boneIndex].localScale = GetMirrorBoneLocalScale(mbi, mt.localScale);
                        if (!values.ContainsKey(mbi))
                        {
                            values.Add(mbi, new TransformPoseSave.SaveData());
                            var t = editBones[boneIndex].transform;
                            values[mbi].localPosition = GetMirrorBoneLocalPosition(boneIndex, t.localPosition);
                            values[mbi].localRotation = GetMirrorBoneLocalRotation(boneIndex, t.localRotation);
                            values[mbi].localScale = GetMirrorBoneLocalScale(boneIndex, t.localScale);
                        }
                    }
                    else
                    {
                        var t = editBones[boneIndex].transform;
                        values[boneIndex].localPosition = GetMirrorBoneLocalPosition(boneIndex, t.localPosition);
                        values[boneIndex].localRotation = GetMirrorBoneLocalRotation(boneIndex, t.localRotation);
                        values[boneIndex].localScale = GetMirrorBoneLocalScale(boneIndex, t.localScale);
                    }
                }
                foreach (var pair in values)
                {
                    var bi = pair.Key;
                    if (bi == rootMotionBoneIndex) continue;
                    var mbi = mirrorBoneIndexes[bi];
                    if (mbi >= 0)
                    {
                        if (IsHaveAnimationCurveTransformPosition(bi) || IsHaveAnimationCurveTransformPosition(mbi))
                            SetAnimationCurveTransformPosition(bi, pair.Value.localPosition, false);
                        if (IsHaveAnimationCurveTransformRotation(bi) != URotationCurveInterpolation.Mode.Undefined || IsHaveAnimationCurveTransformRotation(mbi) != URotationCurveInterpolation.Mode.Undefined)
                            SetAnimationCurveTransformRotation(bi, pair.Value.localRotation, false);
                        if (IsHaveAnimationCurveTransformScale(bi) || IsHaveAnimationCurveTransformScale(mbi))
                            SetAnimationCurveTransformScale(bi, pair.Value.localScale, false);
                    }
                    else
                    {
                        if (IsHaveAnimationCurveTransformPosition(bi))
                            SetAnimationCurveTransformPosition(bi, pair.Value.localPosition, false);
                        if (IsHaveAnimationCurveTransformRotation(bi) != URotationCurveInterpolation.Mode.Undefined)
                            SetAnimationCurveTransformRotation(bi, pair.Value.localRotation, false);
                        if (IsHaveAnimationCurveTransformScale(bi))
                            SetAnimationCurveTransformScale(bi, pair.Value.localScale, false);
                    }
                }
            }
            SetUpdateResampleAnimation();
            SetAnimationWindowRefresh(AnimationWindowStateRefreshType.Everything);
            SetSynchroIKtargetAll(true);
            DisableAnimatorRootCorrection();
        }

        public void SelectionHumanoidMirror()
        {
            {
                int[] muscles = SelectionGameObjectsMuscleIndex();
                int[] mirrorMuscles = new int[muscles.Length];
                var values = new float[muscles.Length];
                for (int i = 0; i < muscles.Length; i++)
                {
                    mirrorMuscles[i] = GetMirrorMuscleIndex(muscles[i]);
                    if (mirrorMuscles[i] >= 0)
                        values[i] = GetAnimationCurveAnimatorMuscle(mirrorMuscles[i]);
                }
                for (int i = 0; i < muscles.Length; i++)
                {
                    if (mirrorMuscles[i] < 0)
                    {
                        var hi = HumanTrait.BoneFromMuscle(muscles[i]);
                        if (muscles[i] == HumanTrait.MuscleFromBone(hi, 0) || muscles[i] == HumanTrait.MuscleFromBone(hi, 1))
                        {
                            var value = -GetAnimationCurveAnimatorMuscle(muscles[i]);
                            if (!IsHaveAnimationCurveAnimatorMuscle(muscles[i]) && value == 0f) continue;
                            SetUpdateIKtargetMuscle(muscles[i]);
                            SetAnimationCurveAnimatorMuscle(muscles[i], value);
                        }
                    }
                    else
                    {
                        if (!IsHaveAnimationCurveAnimatorMuscle(muscles[i]) && values[i] == 0f) continue;
                        SetUpdateIKtargetMuscle(muscles[i]);
                        SetAnimationCurveAnimatorMuscle(muscles[i], values[i], !EditorCommon.ArrayContains(muscles, mirrorMuscles[i]));
                    }
                }
            }
            if (humanoidHasTDoF)
            {
                var his = SelectionGameObjectsHumanoidIndex();
                Vector3[] saves = new Vector3[(int)AnimatorTDOFIndex.Total];
                foreach (var hi in his)
                {
                    if (VeryAnimation.HumanBonesAnimatorTDOFIndex[(int)hi] == null) continue;
                    var tdof = VeryAnimation.HumanBonesAnimatorTDOFIndex[(int)hi].index;
                    var mmi = AnimatorTDOFMirrorIndexes[(int)tdof];
                    if (mmi != AnimatorTDOFIndex.None)
                        saves[(int)mmi] = GetAnimationCurveAnimatorTDOF(mmi);
                    else
                        saves[(int)tdof] = GetAnimationCurveAnimatorTDOF(tdof);
                }
                foreach (var hi in his)
                {
                    if (VeryAnimation.HumanBonesAnimatorTDOFIndex[(int)hi] == null) continue;
                    var tdof = VeryAnimation.HumanBonesAnimatorTDOFIndex[(int)hi].index;
                    if (!IsHaveAnimationCurveAnimatorTDOF(tdof)) continue;
                    var mmi = AnimatorTDOFMirrorIndexes[(int)tdof];
                    var vec = Vector3.zero;
                    if (mmi != AnimatorTDOFIndex.None)
                    {
                        vec = Vector3.Scale(saves[(int)mmi], HumanBonesAnimatorTDOFIndex[(int)AnimatorTDOFIndex2HumanBodyBones[(int)mmi]].mirror);
                    }
                    else
                    {
                        vec = saves[(int)tdof];
                        vec.z = -vec.z;
                    }
                    SetUpdateIKtargetTdofIndex(tdof);
                    SetAnimationCurveAnimatorTDOF(tdof, vec, false);
                }
            }
            if (selectionBones.Contains(rootMotionBoneIndex))
            {
                var rootT = GetAnimationCurveAnimatorRootT();
                SetAnimationCurveAnimatorRootT(new Vector3(-rootT.x, rootT.y, rootT.z));
                var rootQ = GetAnimationCurveAnimatorRootQ();
                SetAnimationCurveAnimatorRootQ(new Quaternion(rootQ.x, -rootQ.y, -rootQ.z, rootQ.w));
                SetUpdateIKtargetAll(true);
            }
        }
        public void SelectionHumanoidResetAll()
        {
            {
                var muscles = SelectionGameObjectsMuscleIndex();
                foreach (var muscle in muscles)
                {
                    if (IsHaveAnimationCurveAnimatorMuscle(muscle))
                    {
                        SetUpdateIKtargetMuscle(muscle);
                        SetAnimationCurveAnimatorMuscle(muscle, 0f);
                    }
                }
            }
            foreach (var hi in SelectionGameObjectsHumanoidIndex())
            {
                if (VeryAnimation.HumanBonesAnimatorTDOFIndex[(int)hi] == null) continue;
                if (IsHaveAnimationCurveAnimatorTDOF(HumanBonesAnimatorTDOFIndex[(int)hi].index))
                {
                    SetUpdateIKtargetTdofIndex(HumanBonesAnimatorTDOFIndex[(int)hi].index);
                    SetAnimationCurveAnimatorTDOF(HumanBonesAnimatorTDOFIndex[(int)hi].index, Vector3.zero);
                }
            }
            if (SelectionGameObjectsIndexOf(vaw.gameObject) >= 0)
            {
                if (IsHaveAnimationCurveAnimatorRootT())
                    SetAnimationCurveAnimatorRootT(new Vector3(0, 1, 0));
                if (IsHaveAnimationCurveAnimatorRootQ())
                    SetAnimationCurveAnimatorRootQ(Quaternion.identity);
                SetUpdateIKtargetAll(true);
            }
        }
        public void SelectionGenericMirror()
        {
            var values = new TransformPoseSave.SaveData[selectionBones.Count];
            for (int i = 0; i < selectionBones.Count; i++)
            {
                var mbi = mirrorBoneIndexes[selectionBones[i]];
                if (mbi >= 0)
                {
                    var mt = editBones[mbi].transform;
                    values[i] = new TransformPoseSave.SaveData()
                    {
                        localPosition = GetMirrorBoneLocalPosition(mbi, mt.localPosition),
                        localRotation = GetMirrorBoneLocalRotation(mbi, mt.localRotation),
                        localScale = GetMirrorBoneLocalScale(mbi, mt.localScale),
                    };
                }
                else
                {
                    var bi = selectionBones[i];
                    var t = editBones[bi].transform;
                    values[i] = new TransformPoseSave.SaveData()
                    {
                        localPosition = GetMirrorBoneLocalPosition(bi, t.localPosition),
                        localRotation = GetMirrorBoneLocalRotation(bi, t.localRotation),
                        localScale = GetMirrorBoneLocalScale(bi, t.localScale),
                    };
                }
            }
            for (int i = 0; i < selectionBones.Count; i++)
            {
                var bi = selectionBones[i];
                if (rootMotionBoneIndex >= 0 && bi == rootMotionBoneIndex) continue;
                bool mirror = !(mirrorBoneIndexes[bi] >= 0 && selectionBones.Contains(mirrorBoneIndexes[bi]));
                if (IsHaveAnimationCurveTransformPosition(bi) || values[i].localPosition != boneSaveTransforms[bi].localPosition)
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformPosition(bi, values[i].localPosition, mirror);
                }
                if (IsHaveAnimationCurveTransformRotation(bi) != URotationCurveInterpolation.Mode.Undefined || values[i].localRotation != boneSaveTransforms[bi].localRotation)
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformRotation(bi, values[i].localRotation, mirror);
                }
                if (IsHaveAnimationCurveTransformScale(bi) || values[i].localScale != boneSaveTransforms[bi].localScale)
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformScale(bi, values[i].localScale, mirror);
                }
            }
            if (rootMotionBoneIndex >= 0)
            {
                if (selectionBones.Contains(rootMotionBoneIndex))
                {
                    var rootT = GetAnimationCurveAnimatorRootT();
                    SetAnimationCurveAnimatorRootT(new Vector3(-rootT.x, rootT.y, rootT.z));
                    var rootQ = GetAnimationCurveAnimatorRootQ();
                    SetAnimationCurveAnimatorRootQ(new Quaternion(rootQ.x, -rootQ.y, -rootQ.z, rootQ.w));
                    SetUpdateIKtargetAll(true);
                }
            }
        }
        public void SelectionGenericResetAll()
        {
            foreach (var bi in selectionBones)
            {
                if (rootMotionBoneIndex >= 0 && rootMotionBoneIndex == bi) continue;
                if (IsHaveAnimationCurveTransformPosition(bi))
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformPosition(bi, boneSaveTransforms[bi].localPosition);
                }
                if (IsHaveAnimationCurveTransformRotation(bi) != URotationCurveInterpolation.Mode.Undefined)
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformRotation(bi, boneSaveTransforms[bi].localRotation);
                }
                if (IsHaveAnimationCurveTransformScale(bi))
                {
                    SetUpdateIKtargetBone(bi);
                    SetAnimationCurveTransformScale(bi, boneSaveTransforms[bi].localScale);
                }
            }
            if (rootMotionBoneIndex >= 0)
            {
                if (selectionBones.Contains(rootMotionBoneIndex))
                {
                    if (IsHaveAnimationCurveAnimatorRootT())
                    {
                        SetAnimationCurveAnimatorRootT(boneSaveTransforms[rootMotionBoneIndex].localPosition);
                        SetUpdateIKtargetAll(true);
                    }
                    if (IsHaveAnimationCurveAnimatorRootQ())
                    {
                        SetAnimationCurveAnimatorRootQ(boneSaveTransforms[rootMotionBoneIndex].localRotation);
                        SetUpdateIKtargetAll(true);
                    }
                }
            }
        }

        private void ResetAllHaveAnimationCurve()
        {
            transformPoseSave.ResetOriginalTransform();
            blendShapeWeightSave.ResetOriginalWeight();

            #region Humanoid
            if (isHuman)
            {
                SetAnimationCurveAnimatorRootT(new Vector3(0, 1, 0));   //Always create
                SetAnimationCurveAnimatorRootQ(Quaternion.identity);    //Always create
                for (int mi = 0; mi < HumanTrait.MuscleCount; mi++)
                {
                    if (!IsHaveAnimationCurveAnimatorMuscle(mi)) continue;
                    SetAnimationCurveAnimatorMuscle(mi, 0f, false);
                }
                for (var tdof = (AnimatorTDOFIndex)0; tdof < AnimatorTDOFIndex.Total; tdof++)
                {
                    if (!IsHaveAnimationCurveAnimatorTDOF(tdof)) continue;
                    SetAnimationCurveAnimatorTDOF(tdof, Vector3.zero, false);
                }
            }
            #endregion

            #region Generic
            for (int i = 0; i < editBones.Length; i++)
            {
                if (isHuman && humanoidConflict[i]) continue;
                if (!isHuman && rootMotionBoneIndex >= 0 && (i == rootMotionBoneIndex || i == 0)) continue;

                if (IsHaveAnimationCurveTransformPosition(i))
                    SetAnimationCurveTransformPosition(i, boneSaveTransforms[i].localPosition, false);
                if (IsHaveAnimationCurveTransformRotation(i) != URotationCurveInterpolation.Mode.Undefined)
                    SetAnimationCurveTransformRotation(i, boneSaveTransforms[i].localRotation, false);
                if (IsHaveAnimationCurveTransformScale(i))
                    SetAnimationCurveTransformScale(i, boneSaveTransforms[i].localScale, false);
            }
            #endregion

            #region BlendShape
            foreach (var renderer in editGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                if (renderer == null || renderer.sharedMesh == null || renderer.sharedMesh.blendShapeCount <= 0) continue;
                for (int i = 0; i < renderer.sharedMesh.blendShapeCount; i++)
                {
                    var name = renderer.sharedMesh.GetBlendShapeName(i);
                    var weight = blendShapeWeightSave.GetOriginalWeight(renderer, name);
                    if (IsHaveAnimationCurveBlendShape(renderer, name))
                        SetAnimationCurveBlendShape(renderer, name, weight, false);
                }
            }
            #endregion
        }
        private void SetAllChangedAnimationCurve()
        {
            #region Humanoid
            if (isHuman)
            {
                HumanPose hp = new HumanPose();
                GetHumanPose(ref hp);
                SetAnimationCurveAnimatorRootT(hp.bodyPosition);    //Always create
                if (IsHaveAnimationCurveAnimatorRootQ())
                    SetAnimationCurveAnimatorRootQ(hp.bodyRotation);
                for (int i = 0; i < hp.muscles.Length; i++)
                {
                    if (!IsHaveAnimationCurveAnimatorMuscle(i) && hp.muscles[i] == 0f) continue;
                    SetAnimationCurveAnimatorMuscle(i, hp.muscles[i], false);
                }
            }
            #endregion

            #region Generic
            if (!isHuman && rootMotionBoneIndex >= 0)
            {
                var t = editBones[rootMotionBoneIndex].transform;
                SetAnimationCurveAnimatorRootT(t.localPosition);    //Always create
                if (IsHaveAnimationCurveAnimatorRootQ())
                    SetAnimationCurveAnimatorRootQ(t.localRotation);
            }
            for (int i = 0; i < editBones.Length; i++)
            {
                if (isHuman && humanoidConflict[i]) continue;
                if (!isHuman && rootMotionBoneIndex >= 0 && (i == rootMotionBoneIndex || i == 0)) continue;

                var t = editBones[i].transform;
                if (IsHaveAnimationCurveTransformPosition(i) || t.localPosition != boneSaveTransforms[i].localPosition)
                    SetAnimationCurveTransformPosition(i, t.localPosition, false);
                if (IsHaveAnimationCurveTransformRotation(i) != URotationCurveInterpolation.Mode.Undefined || t.localRotation != boneSaveTransforms[i].localRotation)
                    SetAnimationCurveTransformRotation(i, t.localRotation, false);
                //There is an error only on the scale, so roughly check it.
                const float Threshold = 0.1f;
                if (IsHaveAnimationCurveTransformScale(i) ||
                    Mathf.Abs(t.localScale.x - boneSaveTransforms[i].localScale.x) >= Threshold ||
                    Mathf.Abs(t.localScale.y - boneSaveTransforms[i].localScale.y) >= Threshold ||
                    Mathf.Abs(t.localScale.z - boneSaveTransforms[i].localScale.z) >= Threshold)
                {
                    SetAnimationCurveTransformScale(i, t.localScale, false);
                }
            }
            #endregion

            #region BlendShape
            foreach (var renderer in editGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                if (renderer == null || renderer.sharedMesh == null || renderer.sharedMesh.blendShapeCount <= 0) continue;
                for (int i = 0; i < renderer.sharedMesh.blendShapeCount; i++)
                {
                    var name = renderer.sharedMesh.GetBlendShapeName(i);
                    var weight = renderer.GetBlendShapeWeight(i);
                    if (IsHaveAnimationCurveBlendShape(renderer, name) || weight != blendShapeWeightSave.GetOriginalWeight(renderer, name))
                        SetAnimationCurveBlendShape(renderer, name, weight, false);
                }
            }
            #endregion
        }

        private bool IsHaveThisTimeRootAnimationCurveKeyframe(float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            for (int dofIndex = 0; dofIndex < 3; dofIndex++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootT[dofIndex]);
                if (curve == null) continue;
                if (FindKeyframeAtTime(curve, time) >= 0)
                    return true;
            }
            for (int dofIndex = 0; dofIndex < 4; dofIndex++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootQ[dofIndex]);
                if (curve == null) continue;
                if (FindKeyframeAtTime(curve, time) >= 0)
                    return true;
            }
            return false;
        }

        public bool IsHaveAnimationCurveAnimatorRootT()
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootT[i]);
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveAnimatorRootT(float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Vector3.zero;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = isHuman || rootMotionBoneIndex < 0 ? Vector3.zero : boneSaveTransforms[rootMotionBoneIndex].localPosition;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootT[i]);
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveAnimatorRootT(Vector3 value3, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change RootT");
            bool created = false;
            for (int i = 0; i < 3; i++)
            {
                float value = value3[i];
                var binding = AnimationCurveBindingAnimatorRootT[i];
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = isHuman ? new Vector3(0f, 1f, 0f) : (rootMotionBoneIndex >= 0 ? boneSaveTransforms[rootMotionBoneIndex].localPosition : Vector3.zero);
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
            DisableAnimatorRootCorrection();

            #region GenericRootMotion
            if (!isHuman && rootMotionBoneIndex >= 0)
                updateGenericRootMotion = true;
            #endregion
        }

        public bool IsHaveAnimationCurveAnimatorRootQ()
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootQ[i]);
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Quaternion GetAnimationCurveAnimatorRootQ(float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Quaternion.identity;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector4 result = isHuman || rootMotionBoneIndex < 0 ? new Vector4(0, 0, 0, 1) : new Vector4(boneSaveTransforms[rootMotionBoneIndex].localRotation.x, boneSaveTransforms[rootMotionBoneIndex].localRotation.y, boneSaveTransforms[rootMotionBoneIndex].localRotation.z, boneSaveTransforms[rootMotionBoneIndex].localRotation.w);
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorRootQ[i]);
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            if (result.sqrMagnitude > 0f)
            {
                result.Normalize();
                return new Quaternion(result.x, result.y, result.z, result.w);
            }
            else
            {
                return Quaternion.identity;
            }
        }
        public void SetAnimationCurveAnimatorRootQ(Quaternion rotation, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change RootQ");
            bool created = false;
            for (int i = 0; i < 4; i++)
            {
                var binding = AnimationCurveBindingAnimatorRootQ[i];
                float value = rotation[i];
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = isHuman ? Quaternion.identity : (rootMotionBoneIndex >= 0 ? boneSaveTransforms[rootMotionBoneIndex].localRotation : Quaternion.identity);
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
            DisableAnimatorRootCorrection();

            #region GenericRootMotion
            if (!isHuman && rootMotionBoneIndex >= 0)
                updateGenericRootMotion = true;
            #endregion
        }

        public bool IsHaveAnimationCurveAnimatorMotionT()
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMotionT[i]);
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveAnimatorMotionT(float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Vector3.zero;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = Vector3.zero;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMotionT[i]);
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveAnimatorMotionT(Vector3 value3, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change MotionT");
            bool created = false;
            for (int i = 0; i < 3; i++)
            {
                float value = value3[i];
                var binding = AnimationCurveBindingAnimatorMotionT[i];
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = Vector3.zero;
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
        }

        public bool IsHaveAnimationCurveAnimatorMotionQ()
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMotionQ[i]);
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Quaternion GetAnimationCurveAnimatorMotionQ(float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Quaternion.identity;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector4 result = new Vector4(0, 0, 0, 1);
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMotionQ[i]);
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            if (result.sqrMagnitude > 0f)
            {
                result.Normalize();
                return new Quaternion(result.x, result.y, result.z, result.w);
            }
            else
            {
                return Quaternion.identity;
            }
        }
        public void SetAnimationCurveAnimatorMotionQ(Quaternion rotation, float time = -1f)
        {
            if (!isHuman) return;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change MotionQ");
            bool created = false;
            for (int i = 0; i < 4; i++)
            {
                var binding = AnimationCurveBindingAnimatorMotionQ[i];
                float value = rotation[i];
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = Quaternion.identity;
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
        }

        public bool IsHaveAnimationCurveAnimatorIkT(AnimatorIKIndex ikIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorIkT(ikIndex, i));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveAnimatorIkT(AnimatorIKIndex ikIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Vector3.zero;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = Vector3.zero;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorIkT(ikIndex, i));
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveAnimatorIkT(AnimatorIKIndex ikIndex, Vector3 value3, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change IK T");
            bool created = false;
            for (int i = 0; i < 3; i++)
            {
                float value = value3[i];
                var binding = AnimationCurveBindingAnimatorIkT(ikIndex, i);
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = Vector3.zero;
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
        }

        public bool IsHaveAnimationCurveAnimatorIkQ(AnimatorIKIndex ikIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorIkQ(ikIndex, i));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Quaternion GetAnimationCurveAnimatorIkQ(AnimatorIKIndex ikIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Quaternion.identity;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector4 result = new Vector4(0, 0, 0, 1);
            for (int i = 0; i < 4; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorIkQ(ikIndex, i));
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            if (result.sqrMagnitude > 0f)
            {
                result.Normalize();
                return new Quaternion(result.x, result.y, result.z, result.w);
            }
            else
            {
                return Quaternion.identity;
            }
        }
        public void SetAnimationCurveAnimatorIkQ(AnimatorIKIndex ikIndex, Quaternion rotation, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change IK Q");
            bool created = false;
            for (int i = 0; i < 4; i++)
            {
                var binding = AnimationCurveBindingAnimatorIkQ(ikIndex, i);
                float value = rotation[i];
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = Quaternion.identity;
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
        }

        public bool IsHaveAnimationCurveAnimatorTDOF(AnimatorTDOFIndex ikIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorTDOF(ikIndex, i));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveAnimatorTDOF(AnimatorTDOFIndex ikIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return Vector3.zero;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = Vector3.zero;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorTDOF(ikIndex, i));
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveAnimatorTDOF(AnimatorTDOFIndex ikIndex, Vector3 value3, bool mirrorSet = true, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change TDOF");
            bool created = false;
            for (int i = 0; i < 3; i++)
            {
                float value = value3[i];
                var binding = AnimationCurveBindingAnimatorTDOF(ikIndex, i);
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    var defaultValue = Vector3.zero;
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, defaultValue[i]);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), defaultValue[i]);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
            EnableAnimatorRootCorrection();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                var mmi = AnimatorTDOFMirrorIndexes[(int)ikIndex];
                if (mmi != AnimatorTDOFIndex.None)
                    SetAnimationCurveAnimatorTDOF(mmi, Vector3.Scale(value3, HumanBonesAnimatorTDOFIndex[(int)AnimatorTDOFIndex2HumanBodyBones[(int)mmi]].mirror), false, time);
            }
            #endregion
        }

        public bool IsHaveAnimationCurveAnimatorMuscle(int muscleIndex)
        {
            if (!isHuman || muscleIndex < 0)
                return false;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            return GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMuscle(muscleIndex)) != null;
        }
        public float GetAnimationCurveAnimatorMuscle(int muscleIndex, float time = -1f)
        {
            if (!isHuman || muscleIndex < 0)
                return 0f;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return 0f;
            var curve = GetEditorCurveCache(clip, AnimationCurveBindingAnimatorMuscle(muscleIndex));
            if (curve == null) return 0f;
            return curve.Evaluate(uAw.GetCurrentTime());
        }
        public void SetAnimationCurveAnimatorMuscle(int muscleIndex, float value, bool mirrorSet = true, float time = -1f)
        {
            if (!isHuman || muscleIndex < 0)
                return;
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change Muscle");
            bool created = false;
            {
                var binding = AnimationCurveBindingAnimatorMuscle(muscleIndex);
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    curve = new AnimationCurve();
                    AddKeyframe(curve, 0f, 0f);
                    AddKeyframe(curve, Mathf.Max(time, clip.length), 0f);
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    created = true;
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            if (created)
            {
                uAw.Repaint();
                animationWindowSynchroSelection = true;
            }
            SetUpdateResampleAnimation();
            EnableAnimatorRootCorrection();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                var mmi = GetMirrorMuscleIndex(muscleIndex);
                if (mmi >= 0)
                    SetAnimationCurveAnimatorMuscle(mmi, value, false, time);
            }
            #endregion
        }

        public bool IsHaveAnimationCurveTransformPosition(int boneIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingTransformPosition(boneIndex, 0));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveTransformPosition(int boneIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return boneSaveTransforms[boneIndex].localPosition;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = boneSaveTransforms[boneIndex].localPosition;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingTransformPosition(boneIndex, i));
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveTransformPosition(int boneIndex, Vector3 position, bool mirrorSet = true, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            bool removeCurve = false;
            if (isHuman && humanoidConflict[boneIndex])
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the humanoid. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                removeCurve = true;
            }
            else if (rootMotionBoneIndex >= 0 && boneIndex == 0)
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                removeCurve = true;
            }
            else if (!isHuman && rootMotionBoneIndex >= 0 && boneIndex == rootMotionBoneIndex)
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Please edit RootT. '{0}'", editBones[boneIndex].name);
                updateGenericRootMotion = true;
                return;
            }

            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change Transform");
            if (removeCurve)
            {
                for (int i = 0; i < 3; i++)
                {
                    var binding = AnimationCurveBindingTransformPosition(boneIndex, i);
                    AnimationUtility.SetEditorCurve(clip, binding, null);
                }
                uAw.Repaint();
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    float value = position[i];
                    var binding = AnimationCurveBindingTransformPosition(boneIndex, i);
                    var curve = GetEditorCurveCache(clip, binding);
                    if (curve == null)
                    {
                        curve = new AnimationCurve();
                        AnimationUtility.SetEditorCurve(clip, binding, curve);
                        uAw.Repaint();
                        var save = boneSaveTransforms[boneIndex];
                        for (int j = 0; j < 3; j++)
                        {
                            var subbinding = AnimationCurveBindingTransformPosition(boneIndex, j);
                            var subcurve = GetEditorCurveCache(clip, subbinding);
                            if (subcurve == null)
                                return; //This bone transform operation conflicts with the root motion. Therefore, it can not operate.
                            while (subcurve.length > 0)
                                subcurve.RemoveKey(0);
                            AddKeyframe(subcurve, 0f, save.localPosition[j]);
                            AddKeyframe(subcurve, Mathf.Max(time, clip.length), save.localPosition[j]);
                            SetEditorCurveCache(clip, subbinding, subcurve);
                        }
                        animationWindowSynchroSelection = true;
                        curve = GetEditorCurveCache(clip, binding);
                    }
                    var keyIndex = FindKeyframeAtTime(curve, time);
                    if (keyIndex < 0)
                    {
                        keyIndex = AddKeyframe(curve, time, value);
                        SetEditorCurveCache(clip, binding, curve);
                        SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                    }
                    else
                    {
                        var key = curve[keyIndex];
                        key.value = value;
                        curve.MoveKey(keyIndex, key);
                        SetEditorCurveCache(clip, binding, curve);
                    }
                }
            }
            SetUpdateResampleAnimation();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                if (mirrorBoneIndexes[boneIndex] >= 0)
                {
                    var mpos = GetMirrorBoneLocalPosition(boneIndex, position);
                    SetAnimationCurveTransformPosition(mirrorBoneIndexes[boneIndex], mpos, false, time);
                }
            }
            #endregion
        }

        public URotationCurveInterpolation.Mode IsHaveAnimationCurveTransformRotation(int boneIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return URotationCurveInterpolation.Mode.Undefined;

            if (GetEditorCurveCache(clip, AnimationCurveBindingTransformRotation(boneIndex, 0, URotationCurveInterpolation.Mode.RawQuaternions)) != null)
                return URotationCurveInterpolation.Mode.RawQuaternions;

            if (GetEditorCurveCache(clip, AnimationCurveBindingTransformRotation(boneIndex, 0, URotationCurveInterpolation.Mode.RawEuler)) != null)
                return URotationCurveInterpolation.Mode.RawEuler;

            return URotationCurveInterpolation.Mode.Undefined;
        }
        public Quaternion GetAnimationCurveTransformRotation(int boneIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return boneSaveTransforms[boneIndex].localRotation;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            var binding = AnimationCurveBindingTransformRotation(boneIndex, 0, URotationCurveInterpolation.Mode.RawQuaternions);
            var curve = GetEditorCurveCache(clip, binding);
            if (curve != null)
            {
                #region RawQuaternions
                Vector4 result = Vector4.zero;
                result[0] = curve.Evaluate(time);
                for (int i = 1; i < 4; i++)
                {
                    binding = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawQuaternions);
                    curve = GetEditorCurveCache(clip, binding);
                    result[i] = curve.Evaluate(time);
                }
                if (result.sqrMagnitude <= 0f)
                    return boneSaveTransforms[boneIndex].localRotation;
                result.Normalize();
                Quaternion resultQ = Quaternion.identity;
                for (int i = 0; i < 4; i++)
                    resultQ[i] = result[i];
                return resultQ;
                #endregion
            }
            else
            {
                binding = AnimationCurveBindingTransformRotation(boneIndex, 0, URotationCurveInterpolation.Mode.RawEuler);
                curve = GetEditorCurveCache(clip, binding);
                if (curve != null)
                {
                    #region RawEuler
                    Vector3 result = Vector3.zero;
                    for (int i = 0; i < 3; i++)
                    {
                        binding = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawEuler);
                        curve = GetEditorCurveCache(clip, binding);
                        if (curve != null)
                            result[i] = curve.Evaluate(time);
                    }
                    return Quaternion.Euler(result);
                }
                #endregion
            }
            return boneSaveTransforms[boneIndex].localRotation;
        }
        public void SetAnimationCurveTransformRotation(int boneIndex, Quaternion rotation, bool mirrorSet = true, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            bool removeCurve = false;
            if (isHuman && humanoidConflict[boneIndex])
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the humanoid. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                removeCurve = true;
            }
            else if (rootMotionBoneIndex >= 0 && boneIndex == 0)
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Therefore, it can not operate. '{0}'", editBones[boneIndex].name);
                removeCurve = true;
            }
            else if (!isHuman && rootMotionBoneIndex >= 0 && boneIndex == rootMotionBoneIndex)
            {
                EditorCommon.ShowNotification("Conflict");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>This bone transform operation conflicts with the root motion. Please edit RootQ. '{0}'", editBones[boneIndex].name);
                updateGenericRootMotion = true;
                return;
            }

            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change Transform");
            var mode = IsHaveAnimationCurveTransformRotation(boneIndex);
            if (removeCurve)
            {
                if (mode == URotationCurveInterpolation.Mode.RawQuaternions)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        var binding = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawQuaternions);
                        AnimationUtility.SetEditorCurve(clip, binding, null);
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var binding = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawEuler);
                        AnimationUtility.SetEditorCurve(clip, binding, null);
                    }
                }
                uAw.Repaint();
            }
            else
            {
                if (mode == URotationCurveInterpolation.Mode.Undefined)
                {
                    mode = URotationCurveInterpolation.Mode.RawQuaternions;
                    var save = boneSaveTransforms[boneIndex];
                    for (int i = 0; i < 4; i++)
                    {
                        var curve = new AnimationCurve();
                        AddKeyframe(curve, 0f, save.localRotation[i]);
                        AddKeyframe(curve, clip.length, save.localRotation[i]);
                        AnimationUtility.SetEditorCurve(clip, AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawQuaternions), curve);
                    }
                    uAw.Repaint();
                    for (int i = 0; i < 4; i++)
                    {
                        var binding = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawQuaternions);
                        var curve = GetEditorCurveCache(clip, binding);
                        if (curve == null)
                            return; //This bone transform operation conflicts with the root motion. Therefore, it can not operate.
                        while (curve.length > 0)
                            curve.RemoveKey(0);
                        AddKeyframe(curve, 0f, save.localRotation[i]);
                        var end = Mathf.Max(time, clip.length);
                        if (end == 0f) end = 1f;
                        AddKeyframe(curve, end, save.localRotation[i]);
                        SetEditorCurveCache(clip, binding, curve);
                    }
                    animationWindowSynchroSelection = true;
                }
                if (mode == URotationCurveInterpolation.Mode.RawQuaternions)
                {
                    #region RawQuaternions
                    {
                        EditorCurveBinding[] bindings = new EditorCurveBinding[4];
                        AnimationCurve[] curves = new AnimationCurve[4];
                        for (int i = 0; i < 4; i++)
                        {
                            bindings[i] = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawQuaternions);
                            curves[i] = GetEditorCurveCache(clip, bindings[i]);
                        }
                        #region FixReverseRotation
                        {
                            Quaternion beforeRotation;
                            {
                                var beforeTime = 0f;
                                for (int i = 0; i < 4; i++)
                                {
                                    var index = FindBeforeNearKeyframeAtTime(curves[i], time);
                                    if (index >= 0)
                                        beforeTime = Mathf.Max(beforeTime, curves[i][index].time);
                                }
                                if (beforeTime == time)
                                {
                                    beforeTime = clip.length;
                                    for (int i = 0; i < 4; i++)
                                    {
                                        if (curves[i].length == 0) continue;
                                        for (int j = 0; j < curves[i].length; j++)
                                        {
                                            if (curves[i][j].time > time)
                                            {
                                                beforeTime = Mathf.Min(beforeTime, curves[i][j].time);
                                                break;
                                            }
                                        }
                                    }
                                }
                                Vector4 result = Vector4.zero;
                                for (int i = 0; i < 4; i++)
                                    result[i] = curves[i].Evaluate(beforeTime);
                                result.Normalize();
                                beforeRotation = Quaternion.identity;
                                for (int i = 0; i < 4; i++)
                                    beforeRotation[i] = result[i];
                            }
                            var rot = rotation * Quaternion.Inverse(beforeRotation);
                            if (rot.w < 0f)
                            {
                                for (int i = 0; i < 4; i++)
                                    rotation[i] = -rotation[i];
                            }
                        }
                        #endregion
                        for (int i = 0; i < 4; i++)
                        {
                            float value = rotation[i];
                            var keyIndex = FindKeyframeAtTime(curves[i], time);
                            //There must be at least two keyframes. If not, an error will occur.[AnimationUtility.GetEditorCurve]
                            Action ErrorAvoidance = () =>
                            {
                                while (curves[i].length < 2)
                                {
                                    var addTime = 0f;
                                    if (time != 0f) addTime = 0f;
                                    else if (clip.length != 0f) addTime = clip.length;
                                    else addTime = 1f;
                                    AddKeyframe(curves[i], addTime, curves[i].Evaluate(addTime));
                                }
                            };
                            if (keyIndex < 0)
                            {
                                keyIndex = AddKeyframe(curves[i], time, value);
                                ErrorAvoidance();
                                SetEditorCurveCache(clip, bindings[i], curves[i]);
                                SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                            }
                            else
                            {
                                var key = curves[i][keyIndex];
                                key.value = value;
                                curves[i].MoveKey(keyIndex, key);
                                ErrorAvoidance();
                                SetEditorCurveCache(clip, bindings[i], curves[i]);
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region RawEuler
                    {
                        EditorCurveBinding[] bindings = new EditorCurveBinding[3];
                        AnimationCurve[] curves = new AnimationCurve[3];
                        for (int i = 0; i < 3; i++)
                        {
                            bindings[i] = AnimationCurveBindingTransformRotation(boneIndex, i, URotationCurveInterpolation.Mode.RawEuler);
                            curves[i] = GetEditorCurveCache(clip, bindings[i]);
                        }
                        var eulerAngles = rotation.eulerAngles;
                        #region FixReverseRotation
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                float beforeTime = 0f;
                                var index = FindBeforeNearKeyframeAtTime(curves[i], time);
                                if (index >= 0)
                                    beforeTime = Mathf.Max(beforeTime, curves[i][index].time);
                                if (beforeTime == time)
                                {
                                    beforeTime = clip.length;
                                    if (curves[i].length > 0)
                                    {
                                        for (int j = 0; j < curves[i].length; j++)
                                        {
                                            if (curves[i][j].time > time)
                                            {
                                                beforeTime = Mathf.Min(beforeTime, curves[i][j].time);
                                                break;
                                            }
                                        }
                                    }
                                }
                                var euler = curves[i].Evaluate(beforeTime);
                                if (Mathf.Abs(eulerAngles[i] - euler) < 180f)
                                    continue;
                                if (euler < eulerAngles[i])
                                    eulerAngles[i] -= 360f;
                                else
                                    eulerAngles[i] += 360f;
                            }
                        }
                        #endregion
                        for (int i = 0; i < 3; i++)
                        {
                            var keyIndex = FindKeyframeAtTime(curves[i], time);
                            if (keyIndex < 0)
                            {
                                keyIndex = AddKeyframe(curves[i], time, eulerAngles[i]);
                                SetEditorCurveCache(clip, bindings[i], curves[i]);
                                SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                            }
                            else
                            {
                                var key = curves[i][keyIndex];
                                key.value = eulerAngles[i];
                                curves[i].MoveKey(keyIndex, key);
                                SetEditorCurveCache(clip, bindings[i], curves[i]);
                            }
                        }
                    }
                    #endregion
                }
            }
            SetUpdateResampleAnimation();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                if (mirrorBoneIndexes[boneIndex] >= 0)
                {
                    var mrotation = GetMirrorBoneLocalRotation(boneIndex, rotation);
                    SetAnimationCurveTransformRotation(mirrorBoneIndexes[boneIndex], mrotation, false, time);
                }
            }
            #endregion
        }

        public bool IsHaveAnimationCurveTransformScale(int boneIndex)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return false;
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingTransformScale(boneIndex, 0));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public Vector3 GetAnimationCurveTransformScale(int boneIndex, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return boneSaveTransforms[boneIndex].localScale;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Vector3 result = boneSaveTransforms[boneIndex].localScale;
            for (int i = 0; i < 3; i++)
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingTransformScale(boneIndex, i));
                if (curve != null)
                {
                    result[i] = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveTransformScale(int boneIndex, Vector3 scale, bool mirrorSet = true, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change Transform");
            for (int i = 0; i < 3; i++)
            {
                float value = scale[i];
                var binding = AnimationCurveBindingTransformScale(boneIndex, i);
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    curve = new AnimationCurve();
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    uAw.Repaint();
                    var save = boneSaveTransforms[boneIndex];
                    for (int j = 0; j < 3; j++)
                    {
                        var subbinding = AnimationCurveBindingTransformScale(boneIndex, j);
                        var subcurve = GetEditorCurveCache(clip, subbinding);
                        if (subcurve == null)
                            return; //This bone transform operation conflicts with the root motion. Therefore, it can not operate.
                        while (subcurve.length > 0)
                            subcurve.RemoveKey(0);
                        AddKeyframe(subcurve, 0f, save.localScale[j]);
                        AddKeyframe(subcurve, Mathf.Max(time, clip.length), save.localScale[j]);
                        SetEditorCurveCache(clip, subbinding, subcurve);
                    }
                    animationWindowSynchroSelection = true;
                    curve = GetEditorCurveCache(clip, binding);
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            SetUpdateResampleAnimation();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                if (mirrorBoneIndexes[boneIndex] >= 0)
                {
                    var mscale = GetMirrorBoneLocalScale(boneIndex, scale);
                    SetAnimationCurveTransformScale(mirrorBoneIndexes[boneIndex], mscale, false, time);
                }
            }
            #endregion
        }

        public bool IsHaveAnimationCurveBlendShape(SkinnedMeshRenderer renderer, string name)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null || renderer.sharedMesh == null) return false;
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingBlendShape(renderer, name));
                if (curve != null)
                    return true;
            }
            return false;
        }
        public float GetAnimationCurveBlendShape(SkinnedMeshRenderer renderer, string name, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null || renderer.sharedMesh == null) return 0f;
            var shapeIndex = renderer.sharedMesh.GetBlendShapeIndex(name);
            if (shapeIndex < 0) return 0f;
            if (time < 0f)
                time = uAw.GetCurrentTime();
            float result = renderer.GetBlendShapeWeight(shapeIndex);
            {
                var curve = GetEditorCurveCache(clip, AnimationCurveBindingBlendShape(renderer, name));
                if (curve != null)
                {
                    result = curve.Evaluate(time);
                }
            }
            return result;
        }
        public void SetAnimationCurveBlendShape(SkinnedMeshRenderer renderer, string name, float value, bool mirrorSet = true, float time = -1f)
        {
            var clip = uAw.GetSelectionAnimationClip();
            if (clip == null || renderer.sharedMesh == null) return;
            if ((clip.hideFlags & HideFlags.NotEditable) != HideFlags.None)
            {
                EditorCommon.ShowNotification("Read-Only");
                Debug.LogErrorFormat("<color=blue>[Very Animation]</color>The animation clip is read-only. '{0}'", clip.name);
                return;
            }
            var shapeIndex = renderer.sharedMesh.GetBlendShapeIndex(name);
            if (shapeIndex < 0) return;
            BeginChangeAnimationCurve();
            if (time < 0f)
                time = uAw.GetCurrentTime();
            Undo.RegisterCompleteObjectUndo(clip, "Change Transform");
            {
                var binding = AnimationCurveBindingBlendShape(renderer, name);
                var curve = GetEditorCurveCache(clip, binding);
                if (curve == null)
                {
                    curve = new AnimationCurve();
                    AnimationUtility.SetEditorCurve(clip, binding, curve);
                    uAw.Repaint();
                    {
                        AddKeyframe(curve, 0f, 0f);
                        AddKeyframe(curve, Mathf.Max(time, clip.length), 0f);
                        SetEditorCurveCache(clip, binding, curve);
                    }
                    animationWindowSynchroSelection = true;
                    curve = GetEditorCurveCache(clip, binding);
                }
                var keyIndex = FindKeyframeAtTime(curve, time);
                if (keyIndex < 0)
                {
                    keyIndex = AddKeyframe(curve, time, value);
                    SetEditorCurveCache(clip, binding, curve);
                    SetAnimationWindowRefresh(AnimationWindowStateRefreshType.CurvesOnly);
                }
                else
                {
                    var key = curve[keyIndex];
                    key.value = value;
                    curve.MoveKey(keyIndex, key);
                    SetEditorCurveCache(clip, binding, curve);
                }
            }
            SetUpdateResampleAnimation();

            #region Mirror
            if (mirrorEnable && mirrorSet)
            {
                Dictionary<string, string> nameTable;
                if (mirrorBlendShape.TryGetValue(renderer, out nameTable))
                {
                    string mirrorName;
                    if (nameTable.TryGetValue(name, out mirrorName))
                    {
                        SetAnimationCurveBlendShape(renderer, mirrorName, value, false, time);
                    }
                }
            }
            #endregion
        }
        #endregion
    }
}
