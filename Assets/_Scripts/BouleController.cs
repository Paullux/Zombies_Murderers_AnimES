﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouleController : MonoBehaviour {

    // Use this for initialization
    public AudioClip LastSceneVoice;
    public bool first = true;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y <= 3)
        {
            transform.position += Vector3.up * Time.deltaTime;
            transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
            GetComponent<Light>().range += 0.01f;
        }
        else
        {
            StartCoroutine("LastScene");
        }
    }
    IEnumerator LastScene()
    {
        yield return new WaitForSeconds(10f);
        if (first)
        {
            GetComponent<AudioSource>().PlayOneShot(LastSceneVoice);
            first = false;
        }
    }
}
